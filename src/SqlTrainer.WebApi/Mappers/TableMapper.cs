using SqlTrainer.WebApi.Dtos.Attributes;
using SqlTrainer.WebApi.Dtos.Tables;

namespace SqlTrainer.WebApi.Mappers;

public static class TableMapper
{
    public static Domain.Models.Table Map(this AddTableDto dto)
    {
        var table = new Domain.Models.Table
        {
            Name = dto.Name,
            DatabaseId = dto.DatabaseId
        };
        table.WithAttributes(dto.Attributes.Select(a => a.Map(table.Id)));
        return table;
    }

    public static Domain.Models.Table Map(this UpdateTableDto dto)
    {
        var table = new Domain.Models.Table(dto.Id)
        {
            Name = dto.Name,
            DatabaseId = dto.DatabaseId
        };
        table.WithAttributes(dto.Attributes.Select(a => a.Map(table.Id)));
        return table;
    }
    
    public static GetTableDto? Map(this Domain.Models.Table? table) => table is null ? null : new GetTableDto
    {
        Id = table.Id,
        Name = table.Name,
        DatabaseId = table.DatabaseId,
        Attributes = table.Attributes?
            .Select(AttributeMapper.Map)
            .Where(a => a is not null)
            .Select(a => a!)
                     ?? Enumerable.Empty<GetAttributeDto>()
    };
}