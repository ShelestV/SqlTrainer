using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.ScheduleTests;

namespace SqlTrainer.WebApi.Mappers;

public static class ScheduleTestMapper
{
    public static ScheduleTest Map(this AddScheduleTestDto dto)
    {
        var scheduleTest = new ScheduleTest
        {
            TestId = dto.TestId,
            StartAt = dto.StartAt,
            FinishAt = dto.FinishAt
        };
        return scheduleTest.WithUserScheduleTests(
            dto.UserIds.Select(id => new UserScheduleTest(scheduleTest.Id, id)).ToList());
    }

    public static ScheduleTest Map(this UpdateScheduleTestDto dto)
    {
        var scheduleTest = new ScheduleTest(dto.Id)
        {
            TestId = dto.TestId,
            StartAt = dto.StartAt,
            FinishAt = dto.FinishAt
        };
        return scheduleTest.WithUserScheduleTests(
            dto.UserIds.Select(id => new UserScheduleTest(scheduleTest.Id, id)).ToList());
    }

    public static GetScheduleTestDto? Map(this ScheduleTest? scheduleTest) => scheduleTest is null ? null : new GetScheduleTestDto
    {
        Id = scheduleTest.Id,
        TestId = scheduleTest.TestId,
        StartAt = scheduleTest.StartAt,
        FinishAt = scheduleTest.FinishAt,
        UserScheduleTests = scheduleTest.UserScheduleTests?.Select(ust => new GetUserScheduleTestDto
        {
            ScheduleTestId = ust.ScheduleTestId,
            UserId = ust.UserId
        }) ?? Enumerable.Empty<GetUserScheduleTestDto>()
    };
}