using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.Topics;

namespace SqlTrainer.WebApi.Mappers;

public static class TopicMapper
{
    public static GetTopicDto? Map(this Topic? topic) => topic is null ? null : new GetTopicDto
    {
        Id = topic.Id,
        Name = topic.Name
    };
    
    public static Topic Map(this AddTopicDto dto) => new()
    {
        Name = dto.Name
    };
    
    public static Topic Map(this UpdateTopicDto dto) => new(dto.Id)
    {
        Name = dto.Name
    };
}