using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.Questions;
using SqlTrainer.WebApi.Dtos.Tests;

namespace SqlTrainer.WebApi.Mappers;

public static class TestMapper
{
    public static Test Map(this AddTestDto dto)
    {
        var test = new Test
        {
            Name = dto.Name,
            CreatedAt = dto.CreatedAt
        };
        return test.WithTestQuestions(dto.Questions
            .Select(q => new TestQuestion(test.Id, q.QuestionId) { MaxMark = q.MaxMark }).ToList());
    }

    public static Test Map(this UpdateTestDto dto)
    {
        var test = new Test(dto.Id)
        {
            Name = dto.Name,
            CreatedAt = DateTimeOffset.UtcNow
        };
        return test.WithTestQuestions(dto.Questions
            .Select(q => new TestQuestion(test.Id, q.QuestionId) { MaxMark = q.MaxMark }).ToList());
    }
    
    public static GetTestDto? Map(this Test? test) => test is null ? null : new GetTestDto
    {
        Id = test.Id,
        Name = test.Name,
        TestQuestions = test.TestQuestions?.Select(tq => new GetTestQuestionDto
        {
            TestId = test.Id,
            QuestionId = tq.QuestionId,
            MaxMark = tq.MaxMark,
            Question = tq.Question is null ? null : new GetQuestionDto
            {
                Id = tq.QuestionId,
                Body = tq.Question.Body,
                Complexity = tq.Question.Complexity,
                DatabaseId = tq.Question.DatabaseId,
                TopicId = tq.Question.TopicId,
                CorrectAnswer = tq.Question.CorrectAnswer is null ? null : new GetQuestionCorrectAnswerDto
                {
                    Id = tq.Question.CorrectAnswer.Id,
                    Body = tq.Question.CorrectAnswer.Body
                }
            }
        }) ?? Enumerable.Empty<GetTestQuestionDto>()
    };
}