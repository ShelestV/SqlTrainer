using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.Roles;

namespace SqlTrainer.WebApi.Mappers;

public static class RoleMapper
{
    public static GetRoleDto? Map(this Role? role) => role is null ? null : new GetRoleDto
    {
        Id = role.Id,
        Name = role.Name
    };
}