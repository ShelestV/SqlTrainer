using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.Questions;

namespace SqlTrainer.WebApi.Mappers;

public static class QuestionMapper
{
    public static GetQuestionDto? Map(this Question? question) => question is null ? null : new GetQuestionDto
    {
        Id = question.Id,
        Body = question.Body,
        Complexity = question.Complexity,
        TopicId = question.TopicId,
        DatabaseId = question.DatabaseId,
        CorrectAnswer = question.CorrectAnswer is null ? null : new GetQuestionCorrectAnswerDto
        {
            Id = question.CorrectAnswer.Id,
            Body = question.CorrectAnswer.Body
        }
    };
    
    public static Question Map(this AddQuestionDto dto)
    {
        var question = new Question
        {
            Body = dto.Body,
            Complexity = dto.Complexity,
            TopicId = dto.TopicId,
            DatabaseId = dto.DatabaseId
        };

        return question.WithCorrectAnswer(new CorrectAnswer
        {
            Body = dto.Body,
            QuestionId = question.Id
        });
    }
    
    public static Question Map(this UpdateQuestionDto dto)
    {
        var question = new Question(dto.Id)
        {
            Body = dto.Body,
            Complexity = dto.Complexity,
            TopicId = dto.TopicId,
            DatabaseId = dto.DatabaseId
        };

        return question.WithCorrectAnswer(new CorrectAnswer(dto.AnswerId)
        {
            Body = dto.AnswerBody,
            QuestionId = question.Id
        });
    }
}