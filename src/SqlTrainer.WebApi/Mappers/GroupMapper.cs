using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.Groups;

namespace SqlTrainer.WebApi.Mappers;

public static class GroupMapper
{
    public static GetGroupDto? Map(this Group? group) => group is null ? null : new GetGroupDto
    {
        Id = group.Id,
        Name = group.Name,
        Users = group.Users?.Select(user => new GetGroupUserDto
        {
            Id = user.Id,
            Name = user.Name,
            Email = user.Email
        }).ToList() ?? Enumerable.Empty<GetGroupUserDto>()
    };
    
    public static Group Map(this AddGroupDto dto) => new()
    {
        Name = dto.Name
    };
    
    public static Group Map(this UpdateGroupDto dto) => new(dto.Id)
    {
        Name = dto.Name
    };
}