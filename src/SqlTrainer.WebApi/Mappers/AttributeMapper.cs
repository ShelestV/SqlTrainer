using SqlTrainer.WebApi.Dtos.Attributes;
using SqlTrainer.WebApi.Dtos.Tables;

namespace SqlTrainer.WebApi.Mappers;

public static class AttributeMapper
{
    public static Domain.Models.Attribute Map(this AddAttributeDto dto, Guid tableId) => new()
    {
        Name = dto.Name,
        Type = dto.Type,
        IsPrimaryKey = dto.IsPrimaryKey,
        IsNotNull = dto.IsNotNull,
        IsUnique = dto.IsUnique,
        DefaultValue = dto.DefaultValue,
        ForeignKeyId = dto.ForeignKeyId,
        VarcharNumberOfSymbols = dto.VarcharNumberOfSymbols,
        Order = dto.Order,
        TableId = tableId
    };

    public static Domain.Models.Attribute Map(this UpdateAttributeDto dto, Guid tableId) => new(dto.Id ?? Guid.Empty)
    {
        Name = dto.Name,
        Type = dto.Type,
        IsPrimaryKey = dto.IsPrimaryKey,
        IsNotNull = dto.IsNotNull,
        IsUnique = dto.IsUnique,
        DefaultValue = dto.DefaultValue,
        ForeignKeyId = dto.ForeignKeyId,
        VarcharNumberOfSymbols = dto.VarcharNumberOfSymbols,
        Order = dto.Order,
        TableId = tableId,
        IsNew = !dto.Id.HasValue || dto.Id.Value == Guid.Empty
    };
    
    public static GetAttributeDto? Map(Domain.Models.Attribute? attribute) => attribute is null ? null : new GetAttributeDto
    {
        Id = attribute.Id,
        Name = attribute.Name,
        Type = attribute.Type,
        VarcharNumberOfSymbols = attribute.VarcharNumberOfSymbols,
        TableId = attribute.TableId,
        IsPrimaryKey = attribute.IsPrimaryKey,
        IsNotNull = attribute.IsNotNull,
        IsUnique = attribute.IsUnique,
        DefaultValue = attribute.DefaultValue,
        ForeignKeyId = attribute.ForeignKeyId,
        Order = attribute.Order,
        Table = attribute.Table is null ? null : new GetTableDto
        {
            Id = attribute.TableId,
            Name = attribute.Table.Name,
            DatabaseId = attribute.Table.DatabaseId
        },
        ForeignKey = attribute.ForeignKey is null ? null : new GetForeignKeyDto
        {
            Id = attribute.ForeignKey!.Id,
            Name = attribute.ForeignKey.Name,
            TableId = attribute.ForeignKey.TableId,
            Table = new GetTableDto
            {
                Id = attribute.ForeignKey.Table.Id,
                Name = attribute.ForeignKey.Table.Name,
                DatabaseId = attribute.ForeignKey.Table.DatabaseId
            }
        }
    };
}