using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.Answers;

namespace SqlTrainer.WebApi.Mappers;

public static class AnswerMapper
{
    public static IEnumerable<UserAnswer> Map(this AddAnswersDto dto, Guid userId) =>
        dto.Answers.Select(a => new UserAnswer(dto.ScheduleTestId, a.QuestionId, userId)
        {
            AnsweredAt = dto.AnsweredAt,
            Body = a.Body,
            ProgramMark = 0.0
        });
}