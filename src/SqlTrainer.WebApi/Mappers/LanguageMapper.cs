using SqlTrainer.WebApi.Dtos.Languages;

namespace SqlTrainer.WebApi.Mappers;

public static class LanguageMapper
{
    public static GetLanguageDto Map(this Domain.Models.Language language) => new()
    {
        Id = language.Id,
        Name = language.Name,
        ShortName = language.ShortName
    };
}