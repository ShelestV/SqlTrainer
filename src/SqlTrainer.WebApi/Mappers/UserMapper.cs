using SqlTrainer.WebApi.Dtos.Users;
using SqlTrainer.WebApi.Helpers;

namespace SqlTrainer.WebApi.Mappers;

public static class UserMapper
{
    private const string DataImage = "data:image/";
    private const string Base64 = ";base64,";
    
    public static GetUserDto? Map(this Domain.Models.User? user) => user is null ? null : new GetUserDto
    {
        Id = user.Id,
        Name = user.Name,
        Email = user.Email,
        FaceImage = string.IsNullOrWhiteSpace(user.FaceImage) ? user.FaceImage : GetImage64(user.FaceImage),
        Rate = user.Rate,
        RoleId = user.RoleId,
        GroupId = user.GroupId
    };
    
    public static Domain.Models.User Map(this AddUserDto dto) => new()
    {
        Name = dto.Name,
        Email = dto.Email,
        HashedPassword = PasswordHasher.Hash(dto.Password),
        RoleId = dto.RoleId,
        GroupId = dto.GroupId,
        FaceImage = dto.FaceImage,
        Rate = dto.Rate
    };

    public static Domain.Models.User Map(this UpdateUserDto dto) => new(dto.Id)
    {
        Name = dto.Name,
        Email = dto.Email,
        RoleId = dto.RoleId,
        GroupId = dto.GroupId,
        FaceImage = dto.FaceImage,
        Rate = dto.Rate
    };
    
    private static string GetImage64(string faceImage)
    {
        var base64 = Convert.ToBase64String(File.ReadAllBytes(faceImage));
        var extension = Path.GetExtension(faceImage);
        return DataImage + extension + Base64 + base64;
    }
}