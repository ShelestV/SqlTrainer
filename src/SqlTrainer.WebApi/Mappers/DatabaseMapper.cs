using SqlTrainer.WebApi.Dtos.Attributes;
using SqlTrainer.WebApi.Dtos.Databases;
using SqlTrainer.WebApi.Dtos.Languages;
using SqlTrainer.WebApi.Dtos.Tables;

namespace SqlTrainer.WebApi.Mappers;

public static class DatabaseMapper
{
    public static Domain.Models.Database Map(this AddDatabaseDto dto) => new()
    {
        Name = dto.Name,
        LanguageId = dto.LanguageId
    };

    public static Domain.Models.Database Map(this UpdateDatabaseDto dto) => new(dto.Id)
    {
        Name = dto.Name,
        LanguageId = Guid.Empty
    };
    
    public static GetDatabaseDto? Map(this Domain.Models.Database? database) => database is null ? null : new GetDatabaseDto
    {
        Id = database.Id,
        Name = database.Name,
        LanguageId = database.LanguageId,
        Language = new GetLanguageDto
        {
            Id = database.Language!.Id,
            Name = database.Language.Name,
            ShortName = database.Language.ShortName
        },
        Tables = database.Tables?.Select(t => new GetTableDto
        {
            Id = t.Id,
            Name = t.Name,
            DatabaseId = t.DatabaseId,
            Attributes = t.Attributes?
                .Select(AttributeMapper.Map)
                .Where(a => a is not null)
                .Select(a => a!) 
                         ?? Enumerable.Empty<GetAttributeDto>()
        }) ?? Enumerable.Empty<GetTableDto>()
    };
}