using Domain;
using SqlTrainer.WebApi;
using SqlTrainer.WebApi.Endpoints;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.TestServiceClient.Extensions;
using SqlTrainer.WebApi.UserServiceClient.Extensions;
using SqlTrainer.WebApi.DatabaseServiceClient.Extensions;
using SqlTrainer.WebApi.VerificationServiceClient.Extensions;

var builder = WebApplication.CreateBuilder(args);

var logger = LoggerFactory.Create(loggingBuilder => loggingBuilder
        .SetMinimumLevel(LogLevel.Information)
        .AddConsole())
    .CreateLogger<Program>();

builder.Services.AddUserServices(builder.Configuration);
builder.Services.AddVerificationService(builder.Configuration);
builder.Services.AddTestServices(builder.Configuration);
builder.Services.AddDatabaseServices(builder.Configuration);

const string devCorsPolicy = "DevCorsPolicy";
builder.Services.AddCors(options =>
{
    options.AddPolicy(devCorsPolicy, corsBuilder =>
    {
        corsBuilder
            .SetIsOriginAllowed(origin => new Uri(origin).Host.Equals("localhost"))
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});

builder.Services.AddJwtBearerAuthentication(builder.Configuration);
builder.Services.AddAuthorizationBuilder()
    .AddPolicy(Policies.Names.Admin, policy => policy.RequireRole(Constants.Roles.Admin))
    .AddPolicy(Policies.Names.Student, policy => policy.RequireRole(Constants.Roles.Student))
    .AddPolicy(Policies.Names.AdminTeacher, policy => policy.RequireRole(Policies.AdminTeacherPolicyRoles))
    .AddPolicy(Policies.Names.StudentSimpleUser, policy => policy.RequireRole(Policies.StudentSimpleUserPolicyRoles))
    .AddPolicy(Policies.Names.AdminTeacherStudent, policy => policy.RequireRole(Policies.AdminTeacherStudentPolicyRoles));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

var group = app.MapGroup("/v1");

// User Service
group.MapGroupEndpoints();
group.MapRoleEndpoints();
group.MapUserEndpoints();

// Test Service
group.MapTopicEndpoints();
group.MapQuestionEndpoints();
group.MapTestEndpoints();
group.MapScheduleTestEndpoints();
group.MapAnswerEndpoints();

// Database Service
group.MapLanguageEndpoints();
group.MapDatabaseEndpoints();
group.MapTableEndpoints();
group.MapAttributeEndpoints();
group.MapScriptEndpoints();

app.UseHttpsRedirection();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors(devCorsPolicy);
}

app.UseAuthentication();
app.UseAuthorization();

await app.CreateAdminAsync(logger);

app.Run();
