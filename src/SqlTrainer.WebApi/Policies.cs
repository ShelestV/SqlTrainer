﻿using Domain;

namespace SqlTrainer.WebApi;

public static class Policies
{
    public static class Names
    {
        public const string Admin = "admin_policy";
        public const string Student = "student_policy";
        public const string AdminTeacher = "admin_teacher_policy";
        public const string StudentSimpleUser = "student_simple_user_policy";
        public const string AdminTeacherStudent = "admin_teacher_student_policy";
    }

    public static readonly string[] AdminTeacherPolicyRoles =
    {
        Constants.Roles.Admin,
        Constants.Roles.Teacher
    };
    
    public static readonly string[] StudentSimpleUserPolicyRoles =
    {
        Constants.Roles.Student,
        Constants.Roles.SimpleUser
    };

    public static readonly string[] AdminTeacherStudentPolicyRoles =
    {
        Constants.Roles.Admin,
        Constants.Roles.Teacher,
        Constants.Roles.Student
    };
}