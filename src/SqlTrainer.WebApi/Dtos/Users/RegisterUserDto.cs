﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Users;

public sealed class RegisterUserDto
{
    public required string Name { get; set; }
    [MinLength(6)]
    [MaxLength(50)]
    [EmailAddress]
    public required string Email { get; set; }
    [MinLength(8)]
    [MaxLength(50)]
    public required string Password { get; set; }
}