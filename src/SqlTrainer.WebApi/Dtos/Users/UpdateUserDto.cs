﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Users;

public sealed class UpdateUserDto
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    [MaxLength(50)]
    public string Name { get; set; } = null!;
    [Required]
    [EmailAddress]
    [MinLength(9)]
    [MaxLength(100)]
    public string Email { get; set; } = null!;
    public string? FaceImage { get; set; }
    public double Rate { get; set; }
    [Required]
    public Guid RoleId { get; set; }
    public Guid? GroupId { get; set; }
}