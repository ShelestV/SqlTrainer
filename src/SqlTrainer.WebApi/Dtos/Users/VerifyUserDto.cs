﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Users;

public sealed class VerifyUserDto
{
    [Required]
    public string Base64 { get; set; } = null!;
}