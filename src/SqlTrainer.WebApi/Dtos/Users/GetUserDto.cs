﻿namespace SqlTrainer.WebApi.Dtos.Users;

public sealed class GetUserDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string? FaceImage { get; set; }
    public double Rate { get; set; }
    public Guid RoleId { get; set; }
    public Guid? GroupId { get; set; }
}