﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Users;

public sealed class LoginUserDto
{
    [Required]
    [MinLength(9)]
    [MaxLength(100)]
    [EmailAddress]
    public string Email { get; set; } = null!;
    [Required]
    [MinLength(8)]
    [MaxLength(50)]
    public string Password { get; set; } = null!;
}