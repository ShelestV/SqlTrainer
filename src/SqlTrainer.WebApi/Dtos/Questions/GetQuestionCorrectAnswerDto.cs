namespace SqlTrainer.WebApi.Dtos.Questions;

public sealed class GetQuestionCorrectAnswerDto
{
    public Guid Id { get; set; }
    public string Body { get; set; } = null!;
}