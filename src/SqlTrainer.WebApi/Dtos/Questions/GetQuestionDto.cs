﻿namespace SqlTrainer.WebApi.Dtos.Questions;

public sealed class GetQuestionDto
{
    public Guid Id { get; set; }
    public string Body { get; set; } = null!;
    public int Complexity { get; set; }
    public Guid TopicId { get; set; }
    public Guid DatabaseId { get; set; }
    public GetQuestionCorrectAnswerDto? CorrectAnswer { get; set; }
}