﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Questions;

public sealed class AddQuestionDto
{
    [Required]
    [MinLength(3)]
    [MaxLength(5000)]
    public string Body { get; set; } = null!;
    [Required]
    public int Complexity { get; set; }
    [Required]
    [MinLength(3)]
    [MaxLength(5000)]
    public string AnswerBody { get; set; } = null!;
    [Required]
    public Guid TopicId { get; set; }
    [Required]
    public Guid DatabaseId { get; set; }
}