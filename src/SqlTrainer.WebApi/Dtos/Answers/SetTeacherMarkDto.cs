using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Answers;

public sealed class SetTeacherMarkDto
{
    [Required]
    public Guid ScheduleTestId { get; set; }
    [Required]
    public Guid QuestionId { get; set; }
    [Required]
    public Guid UserId { get; set; }
    [Required]
    public double TeacherMark { get; set; }
}