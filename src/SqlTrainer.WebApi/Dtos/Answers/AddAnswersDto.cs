using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Answers;

public class AddAnswersDto
{
    [Required]
    public Guid ScheduleTestId { get; set; }
    [Required]
    public DateTimeOffset AnsweredAt { get; set; }
    [Required]
    public IEnumerable<AddAnswerDto> Answers { get; set; } = null!;
}