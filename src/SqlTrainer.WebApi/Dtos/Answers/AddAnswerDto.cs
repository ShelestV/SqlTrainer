using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Answers;

public sealed class AddAnswerDto
{
    [Required]
    public Guid QuestionId { get; set; }
    [Required]
    public string Body { get; set; } = null!;
}