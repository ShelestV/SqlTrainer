﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Tests;

public sealed class AddUpdateTestQuestionDto
{
    [Required]
    public Guid QuestionId { get; set; }
    [Required]
    public double MaxMark { get; set; }
}