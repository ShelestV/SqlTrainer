﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Tests;

public sealed class AddTestDto
{
    [Required]
    [MinLength(3)]
    [MaxLength(500)]
    public string Name { get; set; } = null!;
    [Required]
    public DateTimeOffset CreatedAt { get; set; }
    [Required]
    public IEnumerable<AddUpdateTestQuestionDto> Questions { get; set; } = null!;
}