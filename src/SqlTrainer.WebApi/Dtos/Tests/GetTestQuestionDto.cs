using SqlTrainer.WebApi.Dtos.Questions;

namespace SqlTrainer.WebApi.Dtos.Tests;

public sealed class GetTestQuestionDto
{
    public Guid TestId { get; set; }
    public Guid QuestionId { get; set; }
    public double MaxMark { get; set; }
    public GetQuestionDto? Question { get; set; }
}