﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Tests;

public sealed class UpdateTestDto
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    [MinLength(3)]
    [MaxLength(500)]
    public string Name { get; set; } = null!;
    [Required]
    public IEnumerable<AddUpdateTestQuestionDto> Questions { get; set; } = null!;
}