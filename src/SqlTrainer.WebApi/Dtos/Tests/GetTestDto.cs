﻿namespace SqlTrainer.WebApi.Dtos.Tests;

public sealed class GetTestDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public IEnumerable<GetTestQuestionDto> TestQuestions { get; set; } = null!;
}