namespace SqlTrainer.WebApi.Dtos.Languages;

public sealed class GetLanguageDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string ShortName { get; set; } = null!;
}