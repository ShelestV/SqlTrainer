﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Topics;

public class AddTopicDto
{
    [Required]
    [MinLength(3)]
    [MaxLength(500)]
    public string Name { get; set; } = null!;
}