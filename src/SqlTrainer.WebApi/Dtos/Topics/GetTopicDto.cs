﻿namespace SqlTrainer.WebApi.Dtos.Topics;

public sealed class GetTopicDto
{
    public Guid Id { get; set; }
    public required string Name { get; set; }
}