﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Topics;

public sealed class UpdateTopicDto
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    [MinLength(3)]
    [MaxLength(500)]
    public string Name { get; set; } = null!;
}