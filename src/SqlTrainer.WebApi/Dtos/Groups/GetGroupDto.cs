﻿namespace SqlTrainer.WebApi.Dtos.Groups;

public sealed class GetGroupDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public IEnumerable<GetGroupUserDto> Users { get; set; } = null!;
}