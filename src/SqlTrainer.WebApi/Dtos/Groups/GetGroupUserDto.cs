﻿namespace SqlTrainer.WebApi.Dtos.Groups;

public sealed class GetGroupUserDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string Email { get; set; } = null!;
}