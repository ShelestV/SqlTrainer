﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Groups;

public sealed class AddGroupDto
{
    [Required]
    [MinLength(3)]
    [MaxLength(50)]
    public string Name { get; set; } = null!;

}