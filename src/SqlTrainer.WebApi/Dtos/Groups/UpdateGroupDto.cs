﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Groups;

public sealed class UpdateGroupDto
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    [MinLength(3)]
    [MaxLength(50)]
    public string Name { get; set; } = null!;
}