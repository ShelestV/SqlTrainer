namespace SqlTrainer.WebApi.Dtos.UserScheduleTests;

public sealed class SetFinishedDto
{
    public DateTimeOffset FinishedAt { get; init; }
}