using SqlTrainer.WebApi.Dtos.Attributes;

namespace SqlTrainer.WebApi.Dtos.Tables;

public sealed class GetTableDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public Guid DatabaseId { get; set; }
    public IEnumerable<GetAttributeDto>? Attributes { get; set; }
}