using System.ComponentModel.DataAnnotations;
using SqlTrainer.WebApi.Dtos.Attributes;

namespace SqlTrainer.WebApi.Dtos.Tables;

public sealed class UpdateTableDto
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    [MinLength(3)]
    [MaxLength(200)]
    public string Name { get; set; } = null!;
    [Required]
    public Guid DatabaseId { get; set; }
    [Required]
    public IEnumerable<UpdateAttributeDto> Attributes { get; set; } = null!;

}