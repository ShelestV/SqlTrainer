using System.ComponentModel.DataAnnotations;
using SqlTrainer.WebApi.Dtos.Attributes;

namespace SqlTrainer.WebApi.Dtos.Tables;

public sealed class AddTableDto
{
    [Required]
    [MinLength(3)]
    [MaxLength(200)]
    public string Name { get; init; } = null!;
    [Required]
    public Guid DatabaseId { get; init; }
    [Required]
    public IEnumerable<AddAttributeDto> Attributes { get; init; } = null!;
}