﻿using System.Reflection;
using SqlTrainer.WebApi.Application;

namespace SqlTrainer.WebApi.Dtos;

public sealed class FilterPaginationDto
{
    public string? SearchTerm { get; private set; }
    public string? OrderBy { get; private set; }
    public string? OrderByDirection { get; private set; }
    public int? Page { get; private set; }
    public int? PageSize { get; private set; }
    
    public PaginationFilter ToPaginationFilter()
    {
        return new PaginationFilter(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);
    }

    public static ValueTask<FilterPaginationDto?> BindAsync(HttpContext context, ParameterInfo _)
    {
        const string searchTermKey = "search";
        const string orderByKey = "orderBy";
        const string orderByDirectionKey = "direction";
        const string pageKey = "page";
        const string pageSizeKey = "size";

        var dto = new FilterPaginationDto();

        if (context.Request.Query.TryGetValue(searchTermKey, out var searchTerm))
            dto.SearchTerm = searchTerm;
        
        if (context.Request.Query.TryGetValue(orderByKey, out var orderBy))
            dto.OrderBy = orderBy;
        
        if (context.Request.Query.TryGetValue(orderByDirectionKey, out var orderByDirection))
            dto.OrderByDirection = orderByDirection;
        
        if (context.Request.Query.TryGetValue(pageKey, out var pageString) &&
            int.TryParse(pageString, out var page))
            dto.Page = page;
        
        if (context.Request.Query.TryGetValue(pageSizeKey, out var pageSizeString) &&
            int.TryParse(pageSizeString, out var pageSize))
            dto.PageSize = pageSize;
        
        return ValueTask.FromResult<FilterPaginationDto?>(dto);
    }
}