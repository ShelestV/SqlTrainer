using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Scripts;

public sealed class ScriptDto
{
    [Required]
    [MinLength(11)]
    [MaxLength(5000)]
    public string Script { get; set; } = null!;
}