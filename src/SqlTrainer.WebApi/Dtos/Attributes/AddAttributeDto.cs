using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Attributes;

public sealed class AddAttributeDto
{
    [Required]
    [MinLength(3)]
    [MaxLength(200)]
    public string Name { get; set; } = null!;
    [Required]
    [MinLength(3)]
    [MaxLength(50)]
    public string Type { get; set; } = null!;
    public int? VarcharNumberOfSymbols { get; set; }
    [Required]
    public bool IsPrimaryKey { get; set; }
    [Required]
    public bool IsNotNull { get; set; }
    [Required]
    public bool IsUnique { get; set; }
    public string? DefaultValue { get; set; }
    public Guid? ForeignKeyId { get; set; }
    [Required]
    public int Order { get; set; }

}