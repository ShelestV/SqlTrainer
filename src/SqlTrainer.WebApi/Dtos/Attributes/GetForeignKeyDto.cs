using SqlTrainer.WebApi.Dtos.Tables;

namespace SqlTrainer.WebApi.Dtos.Attributes;

public sealed class GetForeignKeyDto
{
    public Guid Id { get; init; }
    public string Name { get; init; } = null!;
    public Guid TableId { get; init; }
    public GetTableDto Table { get; init; } = null!;
}