using SqlTrainer.WebApi.Dtos.Tables;

namespace SqlTrainer.WebApi.Dtos.Attributes;

public sealed class GetAttributeDto
{
    public Guid Id { get; init; }
    public string Name { get; init; } = null!;
    public string Type { get; init; } = null!;
    public bool IsPrimaryKey { get; init; }
    public bool IsNotNull { get; init; }
    public bool IsUnique { get; init; }
    public string? DefaultValue { get; init; }
    public int? VarcharNumberOfSymbols { get; init; }
    public Guid? ForeignKeyId { get; init; }
    public Guid TableId { get; init; }
    public int Order { get; init; }
    public GetForeignKeyDto? ForeignKey { get; init; }
    public GetTableDto? Table { get; init; }
}