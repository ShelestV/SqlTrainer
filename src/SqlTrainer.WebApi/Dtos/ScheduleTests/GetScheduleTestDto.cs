namespace SqlTrainer.WebApi.Dtos.ScheduleTests;

public sealed class GetScheduleTestDto
{
    public Guid Id { get; set; }
    public Guid TestId { get; set; }
    public IEnumerable<GetUserScheduleTestDto> UserScheduleTests { get; set; } = null!;
    public DateTimeOffset StartAt { get; set; }
    public DateTimeOffset FinishAt { get; set; }
}