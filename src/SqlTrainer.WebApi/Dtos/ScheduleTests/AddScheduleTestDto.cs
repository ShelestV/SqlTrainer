using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.ScheduleTests;

public sealed class AddScheduleTestDto
{
    [Required]
    public Guid TestId { get; set; }
    [Required]
    public IEnumerable<Guid> UserIds { get; set; } = null!;
    [Required]
    public DateTimeOffset StartAt { get; set; }
    [Required]
    public DateTimeOffset FinishAt { get; set; }
}