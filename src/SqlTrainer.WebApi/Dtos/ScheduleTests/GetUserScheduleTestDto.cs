using SqlTrainer.WebApi.Dtos.Users;

namespace SqlTrainer.WebApi.Dtos.ScheduleTests;

public sealed class GetUserScheduleTestDto
{
    public Guid ScheduleTestId { get; set; }
    public Guid UserId { get; set; }
    public GetUserDto? User { get; set; }
}