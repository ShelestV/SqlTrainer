using SqlTrainer.WebApi.Dtos.Languages;
using SqlTrainer.WebApi.Dtos.Tables;

namespace SqlTrainer.WebApi.Dtos.Databases;

public sealed class GetDatabaseDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public Guid LanguageId { get; set; }
    public GetLanguageDto? Language { get; set; }
    public IEnumerable<GetTableDto> Tables { get; set; } = null!;
}