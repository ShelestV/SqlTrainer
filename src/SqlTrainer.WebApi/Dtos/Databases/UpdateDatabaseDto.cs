using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Databases;

public sealed class UpdateDatabaseDto
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    [MinLength(1)]
    [MaxLength(200)]
    public string Name { get; set; } = null!;
}