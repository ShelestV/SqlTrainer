using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Databases;

public sealed class AddDatabaseDto
{
    [Required]
    [MinLength(1)]
    [MaxLength(200)]
    public string Name { get; set; } = null!;
    [Required]
    public Guid LanguageId { get; set; }
}