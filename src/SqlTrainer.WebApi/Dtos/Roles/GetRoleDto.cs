﻿namespace SqlTrainer.WebApi.Dtos.Roles;

public sealed class GetRoleDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
}