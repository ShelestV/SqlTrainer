﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Data;

public sealed class DeleteDataDto
{
    [Required]
    public string JsonData { get; set; } = null!;
}