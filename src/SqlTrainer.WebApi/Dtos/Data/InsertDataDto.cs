﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Data;

public sealed class InsertDataDto
{
    [Required]
    public string JsonData { get; set; } = null!;
}