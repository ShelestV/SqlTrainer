﻿using System.ComponentModel.DataAnnotations;

namespace SqlTrainer.WebApi.Dtos.Data;

public sealed class UpdateDataDto
{
    [Required]
    public string OldJsonData { get; set; } = null!;
    [Required]
    public string NewJsonData { get; set; } = null!;
}