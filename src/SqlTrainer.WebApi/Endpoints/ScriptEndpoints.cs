using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Dtos.Scripts;
using SqlTrainer.WebApi.Extensions;

namespace SqlTrainer.WebApi.Endpoints;

public static class ScriptEndpoints
{
    public static void MapScriptEndpoints(this RouteGroupBuilder appGroup)
    {
        appGroup.MapPost("scripts/execute/{databaseId:guid}", ExecuteScriptAsync).RequireAuthorization();
    }

    private static async Task<IResult> ExecuteScriptAsync(HttpContext context, IScriptService scriptService, [FromRoute] Guid databaseId, [FromBody] ScriptDto dto, CancellationToken cancellationToken) =>
        await scriptService.ExecuteScriptAsync(databaseId, dto.Script, cancellationToken)!.ToAspNetResultAsync();
}