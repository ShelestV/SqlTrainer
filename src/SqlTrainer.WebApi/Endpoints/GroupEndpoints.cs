﻿using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Dtos.Groups;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class GroupEndpoints
{
    public static void MapGroupEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/groups").RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPost("/", AddGroupAsync);
        group.MapDelete("/{id:guid}", DeleteGroupAsync);
        group.MapPut("/", UpdateGroupAsync);
        group.MapGet("/{id:guid}", GetGroupAsync);
        group.MapGet("/", GetAllGroupsAsync);
    }

    private static async Task<IResult> AddGroupAsync(IGroupService groupService, [FromBody] AddGroupDto dto, CancellationToken cancellationToken) =>
        await groupService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();

    private static async Task<IResult> DeleteGroupAsync(IGroupService groupService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await groupService.DeleteAsync(id, cancellationToken).ToAspNetResultAsync();

    private static async Task<IResult> UpdateGroupAsync(IGroupService groupService, [FromBody] UpdateGroupDto dto, CancellationToken cancellationToken) =>
        await groupService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();

    private static async Task<IResult> GetGroupAsync(IGroupService groupService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await groupService.GetAsync(id, cancellationToken)!.ToAspNetResultAsync(GroupMapper.Map);
    
    private static async Task<IResult> GetAllGroupsAsync(IGroupService groupService, CancellationToken cancellationToken) =>
        await groupService.GetAllAsync(cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Group>, Group, GetGroupDto>(GroupMapper.Map);
}