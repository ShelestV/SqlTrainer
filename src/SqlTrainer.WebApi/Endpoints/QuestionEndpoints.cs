﻿using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos;
using SqlTrainer.WebApi.Dtos.Questions;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

using Results = Microsoft.AspNetCore.Http.Results;

public static class QuestionEndpoints
{
    public static void MapQuestionEndpoints(this RouteGroupBuilder appGroup)
    {
        var adminTeacherGroup = appGroup.MapGroup("/questions").RequireAuthorization(Policies.Names.AdminTeacher);
        adminTeacherGroup.MapPost("/", AddQuestionAsync);
        adminTeacherGroup.MapDelete("/{id:guid}", DeleteQuestionAsync);
        adminTeacherGroup.MapPut("/", UpdateQuestionAsync);
        adminTeacherGroup.MapGet("/", GetAllQuestionsAsync);
        
        var adminTeacherStudentGroup = appGroup.MapGroup("/questions/by").RequireAuthorization(Policies.Names.AdminTeacherStudent);
        adminTeacherStudentGroup.MapGet("/test/{testId:guid}", GetQuestionByTestIdAsync);
        adminTeacherStudentGroup.MapGet("/schedule/{scheduleId:guid}", GetQuestionByScheduleIdAsync);
    }

    private static async Task<IResult> AddQuestionAsync(IQuestionService questionService, IDatabaseService databaseService, [FromBody] AddQuestionDto dto, CancellationToken cancellationToken) => 
        await CheckDatabaseForExisting(databaseService, dto.DatabaseId, cancellationToken) 
        ?? await questionService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();

    private static async Task<IResult> DeleteQuestionAsync(IQuestionService questionService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await questionService.DeleteAsync(id, cancellationToken).ToAspNetResultAsync();

    private static async Task<IResult> UpdateQuestionAsync(IQuestionService questionService, IDatabaseService databaseService, [FromBody] UpdateQuestionDto dto, CancellationToken cancellationToken) =>
        await CheckDatabaseForExisting(databaseService, dto.DatabaseId, cancellationToken) 
        ?? await questionService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();

    private static async Task<IResult> GetAllQuestionsAsync(IQuestionService questionService, FilterPaginationDto pagination, CancellationToken cancellationToken) =>
        await questionService.GetAllAsync(pagination.ToPaginationFilter(), cancellationToken: cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Question>, Question, GetQuestionDto>(QuestionMapper.Map);

    private static async Task<IResult> GetQuestionByTestIdAsync(HttpContext context, IQuestionService questionService, [FromRoute] Guid testId, FilterPaginationDto pagination, CancellationToken cancellationToken) =>
        await questionService.GetByTestIdAsync(testId, pagination.ToPaginationFilter(), context.IsTeacherOrAdmin(), cancellationToken)
            .ToAspNetResultAsync<IReadOnlyCollection<Question>, Question, GetQuestionDto>(QuestionMapper.Map);

    private static async Task<IResult> GetQuestionByScheduleIdAsync(HttpContext context, IQuestionService questionService, [FromRoute] Guid scheduleId, CancellationToken cancellationToken) =>
        await questionService.GetByScheduleIdAsync(scheduleId, context.IsTeacherOrAdmin(), cancellationToken)
            .ToAspNetResultAsync<IReadOnlyCollection<Question>, Question, GetQuestionDto>(QuestionMapper.Map);

    /// <summary>
    /// Check if database with such id exists
    /// </summary>
    /// <param name="databaseService">Database service</param>
    /// <param name="databaseId">Id of the database</param>
    /// <param name="cancellationToken">To cancel operation</param>
    /// <returns>Returns null in success case</returns>
    private static async Task<IResult?> CheckDatabaseForExisting(IDatabaseService databaseService, Guid databaseId, CancellationToken cancellationToken)
    {
        var getResult = await databaseService.GetAsync(databaseId, cancellationToken);
        if (!getResult.IsFailed)
            return Results.BadRequest(string.Join(", ", getResult.Errors));

        return getResult.IsNotFound ? Results.BadRequest("Database not found") : null;
    }
}