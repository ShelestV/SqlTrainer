using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Dtos.Answers;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

using Results = Microsoft.AspNetCore.Http.Results;

public static class AnswerEndpoints
{
    public static void MapAnswerEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/answers");
        group.MapPost("/", AddAnswersAsync).RequireAuthorization(Policies.Names.StudentSimpleUser);
        group.MapGet("/by-id/{scheduleTestId:guid}", GetStudentAnswersAsync).RequireAuthorization(Policies.Names.Student);
        group.MapGet("/{userId:guid}/{scheduleTestId:guid}", GetUserAnswersAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPut("/", SetTeacherMarkAsync).RequireAuthorization(Policies.Names.AdminTeacher);
    }

    private static async Task<IResult> AddAnswersAsync(HttpContext context, IAnswerService answerService, [FromBody] AddAnswersDto dto, CancellationToken cancellationToken)
    {
        var userId = context.GetUserId();
        if (!userId.HasValue)
            return Results.Unauthorized();
        
        return await answerService.AddRangeAsync(dto.ScheduleTestId, userId.Value, dto.AnsweredAt, dto.Map(userId.Value), cancellationToken).ToAspNetResultAsync();
    }

    private static async Task<IResult> GetStudentAnswersAsync(HttpContext context, IAnswerService answerService, [FromRoute] Guid scheduleTestId, CancellationToken cancellationToken)
    {
        var userId = context.GetUserId();
        if (!userId.HasValue)
            return Results.Unauthorized();
        
        return await answerService.GetByUserAndScheduleTestAsync(userId.Value, scheduleTestId, false, cancellationToken)!.ToAspNetResultAsync();
    }

    private static async Task<IResult> GetUserAnswersAsync(IAnswerService answerService, [FromRoute] Guid userId, [FromRoute] Guid scheduleTestId, CancellationToken cancellationToken) =>
        await answerService.GetByUserAndScheduleTestAsync(userId, scheduleTestId, true, cancellationToken)!.ToAspNetResultAsync();
    
    private static async Task<IResult> SetTeacherMarkAsync(IAnswerService answerService, [FromBody] SetTeacherMarkDto dto, CancellationToken cancellationToken) =>
        await answerService.SetTeacherMarkAsync(dto.ScheduleTestId, dto.QuestionId, dto.UserId, dto.TeacherMark, cancellationToken).ToAspNetResultAsync();
}