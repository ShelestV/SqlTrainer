using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos;
using SqlTrainer.WebApi.Dtos.ScheduleTests;
using SqlTrainer.WebApi.Dtos.UserScheduleTests;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

using Results = Microsoft.AspNetCore.Http.Results;

public static class ScheduleTestEndpoints
{
    public static void MapScheduleTestEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/schedule-tests");
        group.MapPost("/", AddScheduleTestAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapDelete("/{id:guid}", DeleteScheduleTestAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPut("/", UpdateScheduleTestAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapGet("/{id:guid}", GetScheduleTestAsync).RequireAuthorization(Policies.Names.AdminTeacherStudent);
        group.MapGet("/", GetAllScheduleTestsAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapGet("/by-id", GetScheduleTestsForCurrentUserAsync).RequireAuthorization(Policies.Names.Student);
        group.MapPut("/{scheduleTestId:guid}", SetFinishedAsync).RequireAuthorization(Policies.Names.StudentSimpleUser);
        group.MapPut("/{scheduleTestId:guid}/{userId:guid}", SetCheckedByTeacherAsync).RequireAuthorization(Policies.Names.AdminTeacher);
    }
    
    private static async Task<IResult> AddScheduleTestAsync(IScheduleTestService scheduleTestService, [FromBody] AddScheduleTestDto dto, CancellationToken cancellationToken) => 
        await scheduleTestService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> DeleteScheduleTestAsync(IScheduleTestService scheduleTestService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await scheduleTestService.DeleteAsync(id, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> UpdateScheduleTestAsync(IScheduleTestService scheduleTestService, [FromBody] UpdateScheduleTestDto dto, CancellationToken cancellationToken) =>
        await scheduleTestService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> GetAllScheduleTestsAsync(IScheduleTestService scheduleTestService, IUserService userService, FilterPaginationDto pagination, CancellationToken cancellationToken)
    {
        var getResult = await scheduleTestService.GetAllAsync(pagination.ToPaginationFilter(), cancellationToken);
        if (!getResult.IsSuccess)
            return Results.BadRequest(getResult.Errors);

        var scheduleTestDtos = getResult.Value.Select(ScheduleTestMapper.Map).Where(dto => dto is not null).Select(dto => dto!).ToList();
        var users = new List<User>(); 
        foreach (var userScheduleTestDto in scheduleTestDtos.SelectMany(dto => dto.UserScheduleTests))
        {
            var user = users.FirstOrDefault(u => u.Id == userScheduleTestDto.UserId);
            if (user is null)
            {
                var getUserResult = await userService.GetAsync(userScheduleTestDto.UserId, cancellationToken);
                if (getUserResult is { IsSuccess: true, Value: not null })
                    users.Add(getUserResult.Value);
            }

            if (user is not null)
                userScheduleTestDto.User = user.Map();
        }
        
        return Results.Ok(scheduleTestDtos);
    }
    
    private static async Task<IResult> GetScheduleTestAsync(IScheduleTestService scheduleTestService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await scheduleTestService.GetAsync(id, cancellationToken)!.ToAspNetResultAsync(ScheduleTestMapper.Map);
    
    private static async Task<IResult> GetScheduleTestsForCurrentUserAsync(HttpContext context, IScheduleTestService scheduleTestService, FilterPaginationDto pagination, CancellationToken cancellationToken) =>
        await scheduleTestService.GetByUserIdAsync(context.GetUserId()!.Value, pagination.ToPaginationFilter(), cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<ScheduleTest>, ScheduleTest, GetScheduleTestDto>(ScheduleTestMapper.Map);
    
    private static async Task<IResult> SetFinishedAsync(HttpContext context, IUserScheduleTestService userScheduleTestService, [FromRoute] Guid scheduleTestId, [FromBody] SetFinishedDto dto, CancellationToken cancellationToken) =>
        await userScheduleTestService.UpdateAsync(scheduleTestId, context.GetUserId()!.Value, dto.FinishedAt, false, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> SetCheckedByTeacherAsync(IUserScheduleTestService userScheduleTestService, [FromRoute] Guid scheduleTestId, [FromRoute] Guid userId, CancellationToken cancellationToken) =>
        await userScheduleTestService.UpdateAsync(scheduleTestId, userId, null, true, cancellationToken).ToAspNetResultAsync();
}