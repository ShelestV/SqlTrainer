using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Dtos.Languages;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class LanguageEndpoints
{
    public static void MapLanguageEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/languages").RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapGet("/{languageId:guid}", GetLanguageAsync);
        group.MapGet("/", GetAllLanguagesAsync);
    }
    
    private static async Task<IResult> GetLanguageAsync(HttpContext context, ILanguageService languageService, [FromRoute] Guid languageId, CancellationToken cancellationToken) =>
        await languageService.GetAsync(languageId, cancellationToken)!.ToAspNetResultAsync(LanguageMapper.Map!);
    
    private static async Task<IResult> GetAllLanguagesAsync(HttpContext context, ILanguageService languageService, CancellationToken cancellationToken) =>
        await languageService.GetAllAsync(cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Domain.Models.Language>, Domain.Models.Language, GetLanguageDto>(LanguageMapper.Map!);
}