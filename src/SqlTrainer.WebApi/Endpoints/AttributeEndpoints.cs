using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Dtos.Attributes;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class AttributeEndpoints
{
    public static void MapAttributeEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/attributes").RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapGet("/by-table/{tableId:guid}", GetAttributesByTableAsync);
        group.MapGet("/primary-keys/by-database/{databaseId:guid}", GetPrimaryKeysByDatabaseAsync);
    }
    
    private static async Task<IResult> GetAttributesByTableAsync(HttpContext context, IAttributeService attributeService, [FromRoute] Guid tableId, CancellationToken cancellationToken) =>
        await attributeService.GetByTableAsync(tableId, cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Domain.Models.Attribute>, Domain.Models.Attribute, GetAttributeDto>(AttributeMapper.Map);
    
    private static async Task<IResult> GetPrimaryKeysByDatabaseAsync(HttpContext context, IAttributeService attributeService, [FromRoute] Guid databaseId, CancellationToken cancellationToken) =>
        await attributeService.GetPrimaryKeysByDatabaseAsync(databaseId, cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Domain.Models.Attribute>, Domain.Models.Attribute, GetAttributeDto>(AttributeMapper.Map);
}