using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos;
using SqlTrainer.WebApi.Dtos.Data;
using SqlTrainer.WebApi.Dtos.Tables;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class TableEndpoints
{
    public static void MapTableEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/tables").RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPost("/", AddTableAsync);
        group.MapDelete("/{tableId:guid}", DeleteTableAsync);
        group.MapPut("/", UpdateTableAsync);
        group.MapGet("/{tableId:guid}", GetTableAsync);
        group.MapGet("/", GetAllTablesAsync);
        group.MapGet("/by-database/{databaseId:guid}", GetTablesByDatabaseAsync);
        group.MapGet("/{tableId:guid}/data", SelectDataAsync);
        group.MapPost("/{tableId:guid}/data", InsertDataAsync);
        group.MapPut("/{tableId:guid}/data", UpdateDataAsync);
        group.MapDelete("/{tableId:guid}/data", DeleteDataAsync);
    }
    
    private static async Task<IResult> AddTableAsync(HttpContext context, ITableService tableService, [FromBody] AddTableDto dto, CancellationToken cancellationToken) =>
        await tableService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> DeleteTableAsync(HttpContext context, ITableService tableService, [FromRoute] Guid tableId, CancellationToken cancellationToken) =>
        await tableService.DeleteAsync(tableId, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> UpdateTableAsync(HttpContext context, ITableService tableService, [FromBody] UpdateTableDto dto, CancellationToken cancellationToken) => 
        await tableService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> GetTableAsync(HttpContext context, ITableService tableService, [FromRoute] Guid tableId, CancellationToken cancellationToken) =>
        await tableService.GetAsync(tableId, cancellationToken)!.ToAspNetResultAsync(TableMapper.Map);
    
    private static async Task<IResult> GetAllTablesAsync(HttpContext context, ITableService tableService, FilterPaginationDto pagination, CancellationToken cancellationToken) =>
        await tableService.GetAllAsync(pagination.ToPaginationFilter(), cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Table>, Table, GetTableDto>(TableMapper.Map);
    
    private static async Task<IResult> GetTablesByDatabaseAsync(HttpContext context, ITableService tableService, [FromRoute] Guid databaseId, FilterPaginationDto pagination, CancellationToken cancellationToken) =>
        await tableService.GetByDatabaseAsync(databaseId, pagination.ToPaginationFilter(), cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Table>, Table, GetTableDto>(TableMapper.Map);
    
    private static async Task<IResult> SelectDataAsync(HttpContext context, ITableDataService tableDataService, [FromRoute] Guid tableId, CancellationToken cancellationToken) =>
        await tableDataService.SelectDataAsync(tableId, cancellationToken)!.ToAspNetResultAsync();
    
    private static async Task<IResult> InsertDataAsync(HttpContext context, ITableDataService tableDataService, [FromRoute] Guid tableId, [FromBody] InsertDataDto dto, CancellationToken cancellationToken) =>
        await tableDataService.InsertDataAsync(tableId, dto.JsonData, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> UpdateDataAsync(HttpContext context, ITableDataService tableDataService, [FromRoute] Guid tableId, [FromBody] UpdateDataDto dto, CancellationToken cancellationToken) =>
        await tableDataService.UpdateDataAsync(tableId, dto.OldJsonData, dto.NewJsonData, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> DeleteDataAsync(HttpContext context, ITableDataService tableDataService, [FromRoute] Guid tableId, [FromBody] DeleteDataDto dto, CancellationToken cancellationToken) =>
        await tableDataService.DeleteDataAsync(tableId, dto.JsonData, cancellationToken).ToAspNetResultAsync();
}