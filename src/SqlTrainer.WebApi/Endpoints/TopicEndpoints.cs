﻿using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos;
using SqlTrainer.WebApi.Dtos.Topics;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class TopicEndpoints
{
    public static void MapTopicEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/topics").RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPost("/", AddTopicAsync);
        group.MapDelete("/{id:guid}", DeleteTopicAsync);
        group.MapPut("/", UpdateTopicAsync);
        group.MapGet("/", GetAllTopicsAsync);
    }
    
    private static async Task<IResult> AddTopicAsync(ITopicService topicService, [FromBody] AddTopicDto dto, CancellationToken cancellationToken) =>
        await topicService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> DeleteTopicAsync(ITopicService topicService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await topicService.DeleteAsync(id, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> UpdateTopicAsync(ITopicService topicService, [FromBody] UpdateTopicDto dto, CancellationToken cancellationToken) =>
        await topicService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> GetAllTopicsAsync(ITopicService topicService, FilterPaginationDto dto, CancellationToken cancellationToken) =>
        await topicService.GetAllAsync(dto.ToPaginationFilter(), cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Topic>, Topic, GetTopicDto>(TopicMapper.Map);
}