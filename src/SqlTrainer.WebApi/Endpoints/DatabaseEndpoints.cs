using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos;
using SqlTrainer.WebApi.Dtos.Databases;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class DatabaseEndpoints
{
    public static void MapDatabaseEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/databases").RequireAuthorization();
        group.MapPost("/", AddDatabaseAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapDelete("/{databaseId:guid}", DeleteDatabaseAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPut("/", UpdateDatabaseAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapGet("/{databaseId:guid}", GetDatabaseAsync).RequireAuthorization(Policies.Names.AdminTeacherStudent);
        group.MapGet("/", GetAllDatabasesAsync).RequireAuthorization(Policies.Names.AdminTeacher);
    }
    
    private static async Task<IResult> AddDatabaseAsync(HttpContext context, IDatabaseService databaseService, [FromBody] AddDatabaseDto dto, CancellationToken cancellationToken) =>
        await databaseService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> DeleteDatabaseAsync(HttpContext context, IDatabaseService databaseService, [FromRoute] Guid databaseId, CancellationToken cancellationToken) =>
        await databaseService.DeleteAsync(databaseId, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> UpdateDatabaseAsync(HttpContext context, IDatabaseService databaseService, [FromBody] UpdateDatabaseDto dto, CancellationToken cancellationToken) =>
        await databaseService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> GetDatabaseAsync(HttpContext context, IDatabaseService databaseService, [FromRoute] Guid databaseId, CancellationToken cancellationToken) =>
        await databaseService.GetAsync(databaseId, cancellationToken)!.ToAspNetResultAsync(DatabaseMapper.Map);
    
    private static async Task<IResult> GetAllDatabasesAsync(HttpContext context, IDatabaseService databaseService, FilterPaginationDto pagination, CancellationToken cancellationToken) =>
        await databaseService.GetAllAsync(pagination.ToPaginationFilter(), cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Database>, Database, GetDatabaseDto>(DatabaseMapper.Map);
}