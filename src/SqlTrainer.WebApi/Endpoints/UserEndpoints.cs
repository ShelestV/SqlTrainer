﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Dtos;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Dtos.Users;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Helpers;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

using Results = Microsoft.AspNetCore.Http.Results;

public static class UserEndpoints
{
    private const string DataImage = "data:image/";
    private const string Base64 = ";base64,";
 
    public static void MapUserEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/users");
        group.MapPost("/register", RegisterAsync).AllowAnonymous();
        group.MapPost("/login", LoginAsync).AllowAnonymous();
        group.MapGet("/check/{login}", CheckLoginAsync).AllowAnonymous();
        group.MapGet("/by-id", GetCurrentAsync).RequireAuthorization();
        group.MapGet("/{userId:guid}", GetAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapGet("/", GetAllAsync).RequireAuthorization(Policies.Names.Admin);
        group.MapGet("/students", GetAllStudentsAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPost("/", AddAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapDelete("/{userId:guid}", DeleteAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPut("/", UpdateAsync).RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPost("/verify", VerifyAsync).RequireAuthorization(Policies.Names.Student);
    }
    
    private static async Task<IResult> RegisterAsync(IUserService userService, IRoleService roleService, [FromBody] RegisterUserDto dto, CancellationToken cancellationToken)
    {
        var getUserResult = await userService.GetByEmailAsync(dto.Email, cancellationToken);

        if (getUserResult is { IsSuccess: true, Value: not null })
            return Results.BadRequest("Email is used");

        var hashedPassword = PasswordHasher.Hash(dto.Password);
        
        var getRoleResult = await roleService.GetByNameAsync(Domain.Roles.SimpleUser, cancellationToken);

        if (!getRoleResult.IsSuccess)
            return Results.BadRequest(getRoleResult.Errors);

        var user = new Domain.Models.User
        {
            Name = dto.Name,
            Email = dto.Email,
            HashedPassword = hashedPassword,
            RoleId = getRoleResult.Value.Id
        }.WithRole(getRoleResult.Value);
        
        return await userService.AddAsync(user, cancellationToken).ToAspNetResultAsync();
    }

    private static async Task<IResult> LoginAsync(IUserService userService, IConfiguration configuration, [FromBody] LoginUserDto dto, CancellationToken cancellationToken)
    {
        var getResult = await userService.GetByEmailAsync(dto.Email, cancellationToken);
        if (!getResult.IsSuccess)
            return getResult!.ToAspNetResult();
        
        if (getResult.IsNotFound)
            return Results.BadRequest("User not found");

        var hashedPassword = PasswordHasher.Hash(dto.Password);
        if (!getResult.Value.HashedPassword!.Equals(hashedPassword))
            return Results.BadRequest("Incorrect password");

        var user = getResult.Value;
        var role = user.Role!.Name;
        var secretKey = configuration.GetValue<string>("JWT_SECRET_KEY")!;
        var claims = new Dictionary<string, string>
        {
            { "id", user.Id.ToString() },
            { "role", role }
        };
        
        return Results.Ok(JwtHelper.Generate(claims, role, secretKey));
    }
    
    private static async Task<IResult> CheckLoginAsync(IUserService userService, [FromRoute] string login, CancellationToken cancellationToken)
    {
        var getResult = await userService.GetByEmailAsync(login, cancellationToken);
        return getResult.IsSuccess ? Results.BadRequest("Email is used") :
            getResult.IsNotFound ? Results.Ok() : 
            Results.BadRequest(getResult.Errors);
    }
    
    private static async Task<IResult> GetCurrentAsync(HttpContext context, IUserService userService, CancellationToken cancellationToken)
    {
        var userId = context.GetUserId();
        if (userId is null)
            return Results.Unauthorized();
        
        var getResult = await userService.GetAsync(userId.Value, cancellationToken);
        return getResult.IsSuccess ? Results.Ok(getResult.Value) : Results.Unauthorized();
    }

    private static async Task<IResult> GetAsync(IUserService userService, [FromRoute] Guid userId, CancellationToken cancellationToken) =>
        await userService.GetAsync(userId, cancellationToken)!.ToAspNetResultAsync(UserMapper.Map);

    private static async Task<IResult> GetAllAsync(IUserService userService, CancellationToken cancellationToken) =>
        await userService.GetAllAsync(cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Domain.Models.User>, Domain.Models.User, GetUserDto>(UserMapper.Map);
    
    private static async Task<IResult> GetAllStudentsAsync(IUserService userService, CancellationToken cancellationToken) =>
        await userService.GetAllStudentsAsync(cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Domain.Models.User>, Domain.Models.User, GetUserDto>(UserMapper.Map);
    
    private static async Task<IResult> AddAsync(IUserService userService, IVerificationService verificationService, IConfiguration configuration, [FromBody] AddUserDto dto, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(dto.FaceImage))
            return await userService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();

        var baseImagePath = configuration.GetValue<string>("BASE_IMAGE_PATH")!;
        var imagePath = SavePhoto(dto.Name, dto.Email, dto.FaceImage, baseImagePath);

        var faceImageParts = dto.FaceImage.Split(',');

        var verificationDto = new VerificationDto
        {
            Email = dto.Email,
            Base64 = faceImageParts.Length <= 1 ? dto.FaceImage : faceImageParts[1]
        };
        var result = await verificationService.SaveAsync(verificationDto, cancellationToken);

        if (!result.IsSuccess)
            return Results.BadRequest(string.Join(Environment.NewLine, result.Errors));

        dto.FaceImage = imagePath;
        return await userService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    }

    private static async Task<IResult> DeleteAsync(IUserService userService, IVerificationService verificationService, [FromRoute] Guid userId, CancellationToken cancellationToken) 
    {
        var getUserResult = await userService.GetAsync(userId, cancellationToken);
        if (getUserResult.IsFailed)
            return Results.BadRequest(getUserResult.Errors.First());
        else if (getUserResult.IsNotFound)
            return Results.Ok();

        var result = await verificationService.DeleteAsync(getUserResult.Value.Email, cancellationToken);
        if (!result.IsSuccess)
            return Results.BadRequest(string.Join(Environment.NewLine, result.Errors));

        return await userService.DeleteAsync(userId, cancellationToken).ToAspNetResultAsync();
    }

    private static async Task<IResult> UpdateAsync(IUserService userService, IVerificationService verificationService, IConfiguration configuration, [FromBody] UpdateUserDto dto, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(dto.FaceImage))
            return await userService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();

        var baseImagePath = configuration.GetValue<string>("BASE_IMAGE_PATH")!;
        var imagePath = SavePhoto(dto.Name, dto.Email, dto.FaceImage, baseImagePath);

        var faceImageParts = dto.FaceImage.Split(',');

        var verificationDto = new VerificationDto
        {
            Email = dto.Email,
            Base64 = faceImageParts.Length <= 1 ? dto.FaceImage : faceImageParts[1]
        };
        var result = await verificationService.SaveAsync(verificationDto, cancellationToken);

        if (!result.IsSuccess)
            return Results.BadRequest(string.Join(Environment.NewLine, result.Errors));

        dto.FaceImage = imagePath;
        return await userService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    }

    private static async Task<IResult> VerifyAsync(HttpContext context, IVerificationService verificationService, IUserService userService, [FromBody] VerifyUserDto dto, CancellationToken cancellationToken)
    {
        if (context.User.Identity is not ClaimsIdentity identity)
            return Results.Unauthorized();
        
        var claim = identity.FindFirst("id");
        if (claim is null || !Guid.TryParse(claim.Value, out var id))
            return Results.Unauthorized();

        var getResult = await userService.GetAsync(id, cancellationToken);
        if (!getResult.IsSuccess)
            return getResult!.ToAspNetResult();

        if (getResult.IsNotFound)
            return Results.BadRequest("User not found");
        
        var verificationDto = new VerificationDto
        {
            Email = getResult.Value.Email,
            Base64 = dto.Base64
        };
        
        return await verificationService.VerifyAsync(verificationDto, cancellationToken).ToAspNetResultAsync();
    }
    
    private static string SavePhoto(string name, string email, string image, string baseImagePath)
    {
        var extension = image.Substring(DataImage.Length, image.IndexOf(Base64, StringComparison.Ordinal) - DataImage.Length);
        var imageStartIndex = image.IndexOf(Base64, StringComparison.Ordinal) + Base64.Length;
        var imageBase64 = image[imageStartIndex..];


        var emailWithoutDomain = email[..email.IndexOf('@')];
        var imageName = name.Replace(' ', '_') + "_" + emailWithoutDomain;
        var path = baseImagePath + imageName + "." + extension;
        File.WriteAllBytes(path, Convert.FromBase64String(imageBase64));

        return path;
    }
}