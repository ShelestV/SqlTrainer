﻿using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos;
using SqlTrainer.WebApi.Dtos.Tests;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class TestEndpoints
{
    public static void MapTestEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/tests").RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapPost("/", AddTestAsync);
        group.MapDelete("/{id:guid}", DeleteTestAsync);
        group.MapPut("/", UpdateTestAsync);
        group.MapGet("/", GetAllTestsAsync);
        
        appGroup.MapGet("/tests/{id:guid}", GetTestAsync).RequireAuthorization(Policies.Names.AdminTeacherStudent);
    }
    
    private static async Task<IResult> AddTestAsync(ITestService testService, [FromBody] AddTestDto dto, CancellationToken cancellationToken) => 
        await testService.AddAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> DeleteTestAsync(ITestService testService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await testService.DeleteAsync(id, cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> UpdateTestAsync(ITestService testService, [FromBody] UpdateTestDto dto, CancellationToken cancellationToken) =>
        await testService.UpdateAsync(dto.Map(), cancellationToken).ToAspNetResultAsync();
    
    private static async Task<IResult> GetAllTestsAsync(HttpContext context, ITestService testService, FilterPaginationDto pagination, CancellationToken cancellationToken) =>
        await testService.GetAllAsync(pagination.ToPaginationFilter(), includeCorrectAnswers: context.IsTeacherOrAdmin(), cancellationToken: cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Test>, Test, GetTestDto>(TestMapper.Map);
    
    private static async Task<IResult> GetTestAsync(HttpContext context, ITestService testService, [FromRoute] Guid id, CancellationToken cancellationToken) =>
        await testService.GetAsync(id, includeCorrectAnswers: context.IsTeacherOrAdmin(), cancellationToken: cancellationToken)!.ToAspNetResultAsync(TestMapper.Map);
    
    
}