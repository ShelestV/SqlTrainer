﻿using Microsoft.AspNetCore.Mvc;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Domain.Models;
using SqlTrainer.WebApi.Dtos.Roles;
using SqlTrainer.WebApi.Extensions;
using SqlTrainer.WebApi.Mappers;

namespace SqlTrainer.WebApi.Endpoints;

public static class RoleEndpoints
{
    public static void MapRoleEndpoints(this RouteGroupBuilder appGroup)
    {
        var group = appGroup.MapGroup("/roles").RequireAuthorization(Policies.Names.AdminTeacher);
        group.MapGet("/", GetAllAsync);
        group.MapGet("/{name}", GetByNameAsync);
    }
    
    private static async Task<IResult> GetAllAsync(this IRoleService service, CancellationToken cancellationToken) =>
        await service.GetAllAsync(cancellationToken).ToAspNetResultAsync<IReadOnlyCollection<Role>, Role, GetRoleDto>(RoleMapper.Map);
    
    private static async Task<IResult> GetByNameAsync(this IRoleService service, [FromRoute] string name, CancellationToken cancellationToken) =>
        await service.GetByNameAsync(name, cancellationToken)!.ToAspNetResultAsync(RoleMapper.Map);
}