﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using SqlTrainer.WebApi.Application.Dtos;

namespace SqlTrainer.WebApi.Helpers;

public static class JwtHelper
{
    public static TokenDto Generate(IDictionary<string, string> dictClaims, string role, string secretKey)
    {
        var claims = dictClaims.Select(claim => new Claim(claim.Key, claim.Value)).ToList();
        
        var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
        var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

        var jwt = new JwtSecurityToken(
            signingCredentials: signingCredentials,
            claims: claims,
            expires: DateTime.Now.AddHours(6));

        var token = new JwtSecurityTokenHandler().WriteToken(jwt);
        return new TokenDto { Token = token, Role = role };
    }
}