﻿using System.Security.Cryptography;
using System.Text;

namespace SqlTrainer.WebApi.Helpers;

public static class PasswordHasher
{
    public static string Hash(string password)
    {
        var hash = MD5.HashData(Encoding.UTF8.GetBytes(password));
        return Convert.ToBase64String(hash);
    }
}