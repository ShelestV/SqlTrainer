using System.Security.Claims;
using Domain;

namespace SqlTrainer.WebApi.Extensions;

public static class HttpContextExtensions
{
    public static bool IsAdmin(this HttpContext context)
    {
        var roleClaim = context.GetRoleClaim();
        return roleClaim is not null && roleClaim.Value.Equals(Constants.Roles.Admin);
    }
    
    public static bool IsTeacher(this HttpContext context)
    {
        var roleClaim = context.GetRoleClaim();
        return roleClaim is not null && roleClaim.Value.Equals(Constants.Roles.Teacher);
    }
    
    public static bool IsStudent(this HttpContext context)
    {
        var roleClaim = context.GetRoleClaim();
        return roleClaim is not null && roleClaim.Value.Equals(Constants.Roles.Student);
    }
    
    public static bool IsTeacherOrAdmin(this HttpContext context)
    {
        var roleClaim = context.GetRoleClaim();
        var acceptedRoles = new[] { Constants.Roles.Teacher, Constants.Roles.Admin };
        return roleClaim is not null && acceptedRoles.Contains(roleClaim.Value, StringComparer.OrdinalIgnoreCase);
    }

    public static Guid? GetUserId(this HttpContext context)
    {
        if (context.User.Identity is not ClaimsIdentity identity)
            return null;

        var claim = identity.FindFirst("id");
        if (claim is null || !Guid.TryParse(claim.Value, out var id))
            return null;

        return id;
    }

    private static Claim? GetRoleClaim(this HttpContext context)
    {
        var user = context.User.Identity as ClaimsIdentity;
        return user?.Claims.FirstOrDefault(c => c.Type.Equals(ClaimTypes.Role));
    }
}