﻿using Domain;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.Helpers;

namespace SqlTrainer.WebApi.Extensions;

public static class WebApplicationExtensions
{
    internal static async Task CreateAdminAsync(this WebApplication app, ILogger<Program> logger)
    {
        var adminName = app.Configuration.GetValue<string>("ADMIN_NAME")!;
        var adminEmail = app.Configuration.GetValue<string>("ADMIN_EMAIL")!;
        var adminPassword = app.Configuration.GetValue<string>("ADMIN_PASSWORD")!;

        var roleService = app.Services.GetRequiredService<IRoleService>();
        var userService = app.Services.GetRequiredService<IUserService>();
        var getUserResult = await userService.GetByEmailAsync(adminEmail);

        if (getUserResult.IsFailed)
        {
            logger.LogError("Admin user could not be retrieved, because {Errors}", string.Join(", ", getUserResult.Errors));
            throw new Exception("Admin user could not be retrieved");
        }
        
        // Admin exists
        if (!getUserResult.IsNotFound)
        {
            logger.LogInformation("Admin user already exists");
            return;
        }

        logger.LogInformation("Started retrieving an admin role");
        var getRoleResult = await roleService.GetByNameAsync(Constants.Roles.Admin);
        if (getRoleResult.IsFailed)
        {
            logger.LogError("Admin role could not be retrieved, because {Errors}", string.Join(", ", getRoleResult.Errors));
            throw new Exception("Admin role could not be retrieved");
        }

        if (getRoleResult.IsNotFound)
        {
            logger.LogError("Admin role not found");
            throw new Exception("Admin role not found");
        }

        var admin = new Domain.Models.User
        {
            Name = adminName,
            Email = adminEmail,
            HashedPassword = PasswordHasher.Hash(adminPassword),
            RoleId = getRoleResult.Value.Id
        };

        logger.LogInformation("Started adding an admin user");
        var addUserResult = await userService.AddAsync(admin);
        if (!addUserResult.IsSuccess)
        {
            logger.LogError("Admin user could not been created, because {Errors}", string.Join(", ", addUserResult.Errors));
            throw new Exception("Admin user could not been created");
        }
    }
}