﻿using Domain.Models;
using Results;

namespace SqlTrainer.WebApi.Extensions;

using Results = Microsoft.AspNetCore.Http.Results;

public static class ResultsExtensions
{
    public static async Task<IResult> ToAspNetResultAsync(this Task<Result> resultTask) =>
        (await resultTask).ToAspNetResult();
    
    public static async Task<IResult> ToAspNetResultAsync<T>(this Task<Result<T?>> resultTask) =>
        (await resultTask).ToAspNetResult();
    
    public static async Task<IResult> ToAspNetResultAsync<TModel, TDto>(
        this Task<Result<TModel?>> result, Func<TModel?, TDto?> map) where TModel : Model =>
        (await result).ToAspNetResult(map);
    
    public static async Task<IResult> ToAspNetResultAsync<TCollection, TModel, TDto>(
        this Task<Result<TCollection>> result, Func<TModel?, TDto?> map)
        where TCollection : IEnumerable<TModel>
        where TModel : Model =>
        (await result).ToAspNetResult(map);
    
    public static IResult ToAspNetResult(this Result result) =>
        result.IsSuccess ? Results.Ok() : Results.BadRequest(result.Errors);
    
    public static IResult ToAspNetResult<T>(this Result<T?> result) =>
        result.IsSuccess ? Results.Ok(result.Value) : Results.BadRequest(result.Errors);
    
    public static IResult ToAspNetResult<TModel, TDto>(
        this Result<TModel?> result, Func<TModel?, TDto?> map) where TModel : Model =>
        result.IsSuccess ? Results.Ok(map(result.Value)) : Results.BadRequest(result.Errors);

    public static IResult ToAspNetResult<TCollection, TModel, TDto>(
        this Result<TCollection> result, Func<TModel?, TDto?> map)
        where TCollection : IEnumerable<TModel>
        where TModel : Model =>
        result.IsSuccess ? Results.Ok(result.Value.Select(map)) : Results.BadRequest(result.Errors);
}