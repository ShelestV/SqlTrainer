﻿using GrpcHelper.Extensions;
using Results;
using SqlTrainer.UserInfoService;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.UserServiceClient.Services;

public sealed class UserService : IUserService
{
    private readonly UserInfoService.UserService.UserServiceClient userServiceClient;
    
    public UserService(UserInfoService.UserService.UserServiceClient userServiceClient)
    {
        this.userServiceClient = userServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.User user, CancellationToken cancellationToken = default)
    {
        var request = new AddUserRequest
        {
            Name = user.Name,
            Email = user.Email,
            Password = user.HashedPassword, 
            RoleId = user.RoleId.ToString(),
            GroupId = user.GroupId?.ToString(),
            FaceImage = user.FaceImage
        };
        var response = await userServiceClient.AddUserAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new DeleteUserRequest { Id = id.ToString() };
        var response = await userServiceClient.DeleteUserAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.User user, CancellationToken cancellationToken = default)
    {
        var request = new UpdateUserRequest
        {
            Id = user.Id.ToString(),
            Name = user.Name,
            Email = user.Email,
            RoleId = user.RoleId.ToString(),
            GroupId = user.GroupId?.ToString(),
            FaceImage = user.FaceImage
        };
        var response = await userServiceClient.UpdateUserAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<Domain.Models.User>> GetAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new GetUserByIdRequest { Id = id.ToString() };
        var response = await userServiceClient.GetUserByIdAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.User, Map);
    }

    public async Task<Result<Domain.Models.User>> GetByEmailAsync(string email, CancellationToken cancellationToken = default)
    {
        var request = new GetUserByEmailRequest { Email = email };
        var response = await userServiceClient.GetUserByEmailAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.User, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.User>>> GetAllStudentsAsync(CancellationToken cancellationToken = default)
    {
        var request = new GetAllStudentUsersRequest();
        var response = await userServiceClient.GetAllStudentUsersAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Users, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.User>>> GetAllAsync(CancellationToken cancellationToken = default)
    {
        var request = new GetAllUsersRequest();
        var response = await userServiceClient.GetAllUsersAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Users, Map);
    }

    private static Domain.Models.User? Map(User? dto)
    {
        if (dto is null)
            return null;
        
        var user = new Domain.Models.User(Guid.Parse(dto.Id))
        {
            Name = dto.Name,
            Email = dto.Email,
            HashedPassword = dto.Password,
            RoleId = Guid.Parse(dto.RoleId),
            GroupId = dto.GroupId is null ? null : Guid.Parse(dto.GroupId),
            FaceImage = dto.FaceImage,
            Rate = dto.Rate,
        };

        if (dto.Role is not null)
            user.WithRole(new Domain.Models.Role(Guid.Parse(dto.Role.Id)) { Name = dto.Role.Name });

        if (dto.Group is not null)
            user.WithGroup(new Domain.Models.Group(Guid.Parse(dto.Group.Id)) { Name = dto.Group.Name });

        return user;
    }
}