﻿using GrpcHelper.Extensions;
using Results;
using SqlTrainer.UserInfoService;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.UserServiceClient.Services;

public sealed class RoleService : IRoleService
{
    private readonly UserInfoService.RoleService.RoleServiceClient roleServiceClient;
    
    public RoleService(UserInfoService.RoleService.RoleServiceClient roleServiceClient)
    {
        this.roleServiceClient = roleServiceClient;
    }
    
    public async Task<Result<Domain.Models.Role>> GetByNameAsync(string name, CancellationToken cancellationToken = default)
    {
        var request = new GetRoleByNameRequest { Name = name };
        var response = await roleServiceClient.GetRoleByNameAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Role, Map)!;
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Role>>> GetAllAsync(CancellationToken cancellationToken = default)
    {
        var request = new GetAllRolesRequest();
        var response = await roleServiceClient.GetAllRolesAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Roles, Map)!;
    }
    
    private static Domain.Models.Role? Map(Role? role) => role is null ? null : new Domain.Models.Role(Guid.Parse(role.Id))
    {
        Name = role.Name
    };
}