﻿using GrpcHelper.Extensions;
using Results;
using SqlTrainer.UserInfoService;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.UserServiceClient.Services;

public sealed class GroupService : IGroupService
{
    private readonly UserInfoService.GroupService.GroupServiceClient groupServiceClient;
    
    public GroupService(UserInfoService.GroupService.GroupServiceClient groupServiceClient)
    {
        this.groupServiceClient = groupServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.Group group, CancellationToken cancellationToken = default)
    {
        var request = new AddGroupRequest { Name = group.Name };
        var response = await groupServiceClient.AddGroupAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new DeleteGroupRequest { Id = id.ToString() };
        var response = await groupServiceClient.DeleteGroupAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.Group group, CancellationToken cancellationToken = default)
    {
        var request = new UpdateGroupRequest { Id = group.Id.ToString(), Name = group.Name };
        var response = await groupServiceClient.UpdateGroupAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<Domain.Models.Group>> GetAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new GetByIdGroupRequest { Id = id.ToString() };
        var response = await groupServiceClient.GetGroupByIdAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Group, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Group>>> GetAllAsync(CancellationToken cancellationToken = default)
    {
        var request = new GetAllGroupsRequest();
        var response = await groupServiceClient.GetAllGroupsAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Groups, Map);
    }

    private static Domain.Models.Group? Map(Group? dto) => dto is null ? null : new Domain.Models.Group(Guid.Parse(dto.Id))
    {
        Name = dto.Name
    };
}