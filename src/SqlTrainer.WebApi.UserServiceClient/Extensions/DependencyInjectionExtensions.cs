﻿using GrpcHelper.DependencyInjection.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.UserServiceClient.Services;

namespace SqlTrainer.WebApi.UserServiceClient.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddUserServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<IGroupService, GroupService>();
        services.AddTransient<IRoleService, RoleService>();
        services.AddTransient<IUserService, UserService>();
        
        var userInfoUri = configuration.GetValue<string>("USER_INFO_SERVICE_URL")!;
        services.AddNotSecureGrpcClient<UserInfoService.GroupService.GroupServiceClient>(userInfoUri);
        services.AddNotSecureGrpcClient<UserInfoService.RoleService.RoleServiceClient>(userInfoUri);
        services.AddNotSecureGrpcClient<UserInfoService.UserService.UserServiceClient>(userInfoUri);

        return services;
    }
}