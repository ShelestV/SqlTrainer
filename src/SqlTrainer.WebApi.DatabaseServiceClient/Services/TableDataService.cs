﻿using GrpcHelper.Extensions;
using Results;
using SqlTrainer.DatabaseInfoService;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Services;

public sealed class TableDataService : ITableDataService
{
    private readonly DataService.DataServiceClient dataServiceClient;
    
    public TableDataService(DataService.DataServiceClient dataServiceClient)
    {
        this.dataServiceClient = dataServiceClient;
    }
    
    public async Task<Result> InsertDataAsync(Guid tableId, string jsonData, CancellationToken cancellationToken = default)
    {
        var request = new InsertRequest
        {
            TableId = tableId.ToString(),
            JsonData = jsonData
        };
        var response = await dataServiceClient.InsertAsync(request);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateDataAsync(Guid tableId, string oldJsonData, string newJsonData, CancellationToken cancellationToken = default)
    {
        var request = new UpdateRequest
        {
            TableId = tableId.ToString(),
            OldJsonData = oldJsonData,
            NewJsonData = newJsonData
        };
        var response = await dataServiceClient.UpdateAsync(request);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteDataAsync(Guid tableId, string jsonData, CancellationToken cancellationToken = default)
    {
        var request = new DeleteRequest
        {
            TableId = tableId.ToString(),
            JsonData = jsonData
        };
        var response = await dataServiceClient.DeleteAsync(request);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<string>> SelectDataAsync(Guid tableId, CancellationToken cancellationToken = default)
    {
        var request = new SelectRequest
        {
            TableId = tableId.ToString()
        };
        var response = await dataServiceClient.SelectAsync(request, cancellationToken: cancellationToken);
        return response.ToResult<SelectResponse, SelectDto, string>(
            r => r.IsSuccess, 
            r => r.Errors, 
            r => new SelectDto { Json = r.JsonScriptResult },
            r => r?.Json);
    }

    private sealed class SelectDto
    {
        public string? Json { get; set; }
    }
}