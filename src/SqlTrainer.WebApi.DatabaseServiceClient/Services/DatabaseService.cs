using GrpcHelper.Extensions;
using Results;
using SqlTrainer.DatabaseInfoService;
using SqlTrainer.WebApi.Application;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Services;

public sealed class DatabaseService : IDatabaseService
{
    private readonly DatabaseInfoService.DatabaseService.DatabaseServiceClient databaseServiceClient;

    public DatabaseService(DatabaseInfoService.DatabaseService.DatabaseServiceClient databaseServiceClient)
    {
        this.databaseServiceClient = databaseServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.Database database, CancellationToken cancellationToken = default)
    {
        var request = database.ToAddRpcDto();
        var response = await databaseServiceClient.AddAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new DeleteDatabaseRequest { Id = id.ToString() };
        var response = await databaseServiceClient.DeleteAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.Database database, CancellationToken cancellationToken = default)
    {
        var request = database.ToUpdateRpcDto();
        var response = await databaseServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<Domain.Models.Database>> GetAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new GetDatabaseByIdRequest { Id = id.ToString() };
        var response = await databaseServiceClient.GetAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Database, DatabaseMapper.FromRpcDto);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Database>>> GetAllAsync(PaginationFilter pagination, CancellationToken cancellationToken = default)
    {
        var request = new GetAllDatabasesRequest
        {
            SearchTerm = pagination.SearchTerm,
            OrderBy = pagination.OrderBy,
            OrderByDirection = pagination.OrderByDirection,
            Page = pagination.Page,
            PageSize = pagination.PageSize
        };
        var response = await databaseServiceClient.GetAllAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Databases, DatabaseMapper.FromRpcDto)!;
    }
}