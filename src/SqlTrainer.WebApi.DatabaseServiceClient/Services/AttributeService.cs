using GrpcHelper.Extensions;
using Results;
using SqlTrainer.DatabaseInfoService;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Services;

public sealed class AttributeService : IAttributeService
{
    private readonly DatabaseInfoService.AttributeService.AttributeServiceClient attributeServiceClient;

    public AttributeService(DatabaseInfoService.AttributeService.AttributeServiceClient attributeServiceClient)
    {
        this.attributeServiceClient = attributeServiceClient;
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Attribute>>> GetByTableAsync(Guid tableId, CancellationToken cancellationToken)
    {
        var request = new GetAttributesByTableRequest
        {
            TableId = tableId.ToString()
        };
        var response = await attributeServiceClient.GetByTableAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Attributes, AttributeMapper.FromRpcDto);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Attribute>>> GetPrimaryKeysByDatabaseAsync(Guid databaseId, CancellationToken cancellationToken)
    {
        var request = new GetPrimaryKeysByDatabaseRequest
        {
            DatabaseId = databaseId.ToString()
        };
        var response = await attributeServiceClient.GetPrimaryKeysByDatabaseAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Attributes, AttributeMapper.FromRpcDto);
    }
}