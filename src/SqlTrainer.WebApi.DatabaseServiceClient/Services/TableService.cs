using GrpcHelper.Extensions;
using Results;
using SqlTrainer.DatabaseInfoService;
using SqlTrainer.WebApi.Application;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Services;

public sealed class TableService : ITableService
{
    private readonly DatabaseInfoService.TableService.TableServiceClient tableServiceClient;

    public TableService(DatabaseInfoService.TableService.TableServiceClient tableServiceClient)
    {
        this.tableServiceClient = tableServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.Table table, CancellationToken cancellationToken)
    {
        var request = table.ToAddRpcDto();
        var response = await tableServiceClient.AddAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid tableId, CancellationToken cancellationToken)
    {
        var request = new DeleteTableRequest { Id = tableId.ToString() };
        var response = await tableServiceClient.DeleteAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.Table table, CancellationToken cancellationToken)
    {
        var request = table.ToUpdateRpcDto();
        var response = await tableServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<Domain.Models.Table>> GetAsync(Guid tableId, CancellationToken cancellationToken)
    {
        var request = new GetTableByIdRequest { Id = tableId.ToString() };
        var response = await tableServiceClient.GetAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Table, TableMapper.FromRpcDto);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Table>>> GetAllAsync(PaginationFilter pagination, CancellationToken cancellationToken)
    {
        var request = new GetAllTablesRequest
        {
            SearchTerm = pagination.SearchTerm,
            OrderBy = pagination.OrderBy,
            OrderByDirection = pagination.OrderByDirection,
            Page = pagination.Page,
            PageSize = pagination.PageSize
        };
        var response = await tableServiceClient.GetAllAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Tables, TableMapper.FromRpcDto);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Table>>> GetByDatabaseAsync(Guid databaseId, PaginationFilter pagination, CancellationToken cancellationToken)
    {
        var request = new GetTablesByDatabaseRequest
        {
            DatabaseId = databaseId.ToString(),
            SearchTerm = pagination.SearchTerm,
            OrderBy = pagination.OrderBy,
            OrderByDirection = pagination.OrderByDirection,
            Page = pagination.Page,
            PageSize = pagination.PageSize
        };
        var response = await tableServiceClient.GetByDatabaseAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Tables, TableMapper.FromRpcDto);
    }
}