using System.ComponentModel;
using GrpcHelper.Extensions;
using Results;
using SqlTrainer.DatabaseInfoService;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Services;

public sealed class ScriptService : IScriptService
{
    private readonly DatabaseInfoService.ScriptService.ScriptServiceClient scriptServiceClient;
    
    public ScriptService(DatabaseInfoService.ScriptService.ScriptServiceClient scriptServiceClient)
    {
        this.scriptServiceClient = scriptServiceClient;
    }
    
    public async Task<Result<string>> ExecuteScriptAsync(Guid databaseId, string script, CancellationToken cancellationToken = default)
    {
        var result = Result<string>.Create();
        
        var request = new ExecuteRequest
        {
            DatabaseId = databaseId.ToString(),
            Script = script
        };
        var response = await scriptServiceClient.ExecuteAsync(request, cancellationToken: cancellationToken);
        var responseResult = response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Result, FromRpcDto!);
        if (responseResult.IsFailed)
        {
            result.Fail(responseResult.Errors);
            return result;
        }

        if (responseResult.IsNotFound)
        {
            result.Fail("Script result is null");
            return result;
        }
        
        result.Complete(responseResult.Value.ToFinalJson());
        return result;
    }
    
    private static DatabaseHelper.ScriptResults.ScriptResult FromRpcDto(ScriptResult script)
    {
        var scriptResult = new DatabaseHelper.ScriptResults.ScriptResult { Script = script.Script };
        foreach (var obj in script.Objects)
        {
            if (obj is null)
                continue;
            scriptResult.Add(FromRpcDto(obj));
        }
        return scriptResult;
    }

    private static DatabaseHelper.ScriptResults.ScriptResultObject FromRpcDto(ScriptResultObject scriptResultObject)
    {
        var scriptObject = new DatabaseHelper.ScriptResults.ScriptResultObject();
        foreach (var objValue in scriptResultObject.Values)
        {
            if (objValue is null)
                continue;
            scriptObject.Add(FromRpcDto(objValue));
        }
        return scriptObject;
    }

    private static DatabaseHelper.ScriptResults.ScriptResultObjectValue FromRpcDto(ScriptResultObjectValue scriptResultObjectValue)
    {
        var converter = TypeDescriptor.GetConverter(scriptResultObjectValue.CsharpType);
        return new DatabaseHelper.ScriptResults.ScriptResultObjectValue(
            scriptResultObjectValue.Name, 
            converter.ConvertFrom(scriptResultObjectValue.Value)!);
    }
}