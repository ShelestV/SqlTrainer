using GrpcHelper.Extensions;
using Results;
using SqlTrainer.DatabaseInfoService;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Services;

public sealed class LanguageService : ILanguageService
{
    private readonly DatabaseInfoService.LanguageService.LanguageServiceClient languageServiceClient;

    public LanguageService(DatabaseInfoService.LanguageService.LanguageServiceClient languageServiceClient)
    {
        this.languageServiceClient = languageServiceClient;
    }
    
    public async Task<Result<Domain.Models.Language>> GetAsync(Guid languageId, CancellationToken cancellationToken)
    {
        var request = new GetLanguageByIdRequest { Id = languageId.ToString() };
        var response = await languageServiceClient.GetAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Language, LanguageMapper.FromRpcDto);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Language>>> GetAllAsync(CancellationToken cancellationToken)
    {
        var request = new GetAllLanguagesRequest();
        var response = await languageServiceClient.GetAllAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Languages, LanguageMapper.FromRpcDto)!;
    }
}