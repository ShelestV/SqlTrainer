namespace SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

public static class AttributeMapper
{
    public static DatabaseInfoService.AddTableAttributeMessage? ToAddTableRpcDto(this Domain.Models.Attribute? attribute)
    {
        if (attribute is null)
            return null;
        
        return new DatabaseInfoService.AddTableAttributeMessage
        {
            Name = attribute.Name,
            Type = attribute.Type,
            IsPrimaryKey = attribute.IsPrimaryKey,
            IsNotNull = attribute.IsNotNull,
            IsUnique = attribute.IsUnique,
            DefaultValue = attribute.DefaultValue,
            VarcharNumberOfSymbols = attribute.VarcharNumberOfSymbols,
            ForeignKeyId = attribute.ForeignKeyId?.ToString(),
            Order = attribute.Order
        };
    }

    public static DatabaseInfoService.UpdateTableAttributeMessage? ToUpdateTableRpcDto(this Domain.Models.Attribute? attribute)
    {
        if (attribute is null)
            return null;
        
        return new DatabaseInfoService.UpdateTableAttributeMessage
        {
            Id = attribute.IsNew ? null : attribute.Id.ToString(),
            Name = attribute.Name,
            Type = attribute.Type,
            IsPrimaryKey = attribute.IsPrimaryKey,
            IsNotNull = attribute.IsNotNull,
            IsUnique = attribute.IsUnique,
            DefaultValue = attribute.DefaultValue,
            VarcharNumberOfSymbols = attribute.VarcharNumberOfSymbols,
            ForeignKeyId = attribute.ForeignKeyId?.ToString(),
            TableId = attribute.TableId.ToString(),
            Order = attribute.Order
        };
    }
    
    public static Domain.Models.Attribute? FromRpcDto(this SqlTrainer.DatabaseInfoService.Attribute? attribute)
    {
        if (!attribute.CouldMapAttribute(out var attributeId, out var tableId, out var foreignKeyId))
            return null;
        
        var model = new Domain.Models.Attribute(attributeId)
        {
            Name = attribute!.Name,
            Type = attribute.Type,
            IsPrimaryKey = attribute.IsPrimaryKey,
            IsNotNull = attribute.IsNotNull,
            IsUnique = attribute.IsUnique,
            DefaultValue = attribute.DefaultValue,
            ForeignKeyId = foreignKeyId,
            TableId = tableId,
            Order = attribute.Order,
            VarcharNumberOfSymbols = attribute.VarcharNumberOfSymbols
        };

        var table = attribute.Table.FromRpcDto();
        if (table is not null)
            model.WithTable(table);

        var foreignKey = attribute.ForeignKey.FromRpcDto();
        if (foreignKey is not null)
            model.WithForeignKey(foreignKey);

        return model;
    }

    private static bool CouldMapAttribute(
        this SqlTrainer.DatabaseInfoService.Attribute? attribute,
        out Guid attributeId,
        out Guid tableId,
        out Guid? foreignKeyId)
    {
        attributeId = Guid.Empty;
        tableId = Guid.Empty;
        foreignKeyId = null;

        var success = attribute is not null &&
                      Guid.TryParse(attribute.Id, out attributeId) &&
                      Guid.TryParse(attribute.TableId, out tableId);
        
        if (!success)
            return false;

        if (string.IsNullOrWhiteSpace(attribute!.ForeignKeyId))
            return success;
        
        success = Guid.TryParse(attribute.ForeignKeyId, out var fkId);
        foreignKeyId = fkId;
        return success;
    }
}