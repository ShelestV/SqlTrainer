using SqlTrainer.DatabaseInfoService;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

public static class LanguageMapper
{
    public static Domain.Models.Language? FromRpcDto(this Language? language)
    {
        if (language is null)
            return null;

        var model = new Domain.Models.Language(Guid.Parse(language.Id))
        {
            Name = language.Name,
            ShortName = language.ShortName,
            BaseConnectionString = string.Empty
        };

        return model;
    }
}