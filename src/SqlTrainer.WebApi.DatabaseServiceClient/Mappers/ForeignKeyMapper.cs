using SqlTrainer.DatabaseInfoService;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

public static class ForeignKeyMapper
{
    public static Domain.Models.ForeignKey? FromRpcDto(this ForeignKey? foreignKey)
    {
        if (foreignKey is null)
            return null;

        var model = new Domain.Models.ForeignKey(Guid.Parse(foreignKey.Id))
        {
            Name = foreignKey.Name,
            TableId = Guid.Parse(foreignKey.TableId),
            Table = new Domain.Models.Table(Guid.Parse(foreignKey.TableId))
            {
                Name = foreignKey.Table.Name,
                DatabaseId = Guid.Parse(foreignKey.Table.DatabaseId)
            }
        };

        return model;
    }
}