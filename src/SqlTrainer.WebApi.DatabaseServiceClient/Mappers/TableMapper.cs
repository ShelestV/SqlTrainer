using GrpcHelper.Extensions;
using SqlTrainer.DatabaseInfoService;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

public static class TableMapper
{
    public static AddTableRequest ToAddRpcDto(this Domain.Models.Table table) => new()
    {
        Name = table.Name,
        DatabaseId = table.DatabaseId.ToString(),
        Attributes = { table.Attributes?.Select(AttributeMapper.ToAddTableRpcDto) }
    };

    public static UpdateTableRequest ToUpdateRpcDto(this Domain.Models.Table table) => new()
    {
        Id = table.Id.ToString(),
        Name = table.Name,
        Attributes = { table.Attributes?.Select(AttributeMapper.ToUpdateTableRpcDto) }
    };
    
    public static Domain.Models.Table? FromRpcDto(this Table? table)
    {
        if (table is null)
            return null;

        var model = new Domain.Models.Table(Guid.Parse(table.Id))
        {
            Name = table.Name,
            DatabaseId = Guid.Parse(table.DatabaseId)
        };

        var database = table.Database.FromRpcDto();
        if (database is not null)
            model.WithDatabase(database);

        if (table.Attributes is not null)
            model.WithAttributes(table.Attributes.Map(AttributeMapper.FromRpcDto));

        return model;
    }
}