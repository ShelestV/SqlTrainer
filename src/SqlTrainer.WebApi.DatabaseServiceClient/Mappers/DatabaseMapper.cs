using GrpcHelper.Extensions;
using SqlTrainer.DatabaseInfoService;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Mappers;

public static class DatabaseMapper
{
    public static AddDatabaseRequest ToAddRpcDto(this Domain.Models.Database database) => new()
    {
        Name = database.Name,
        LanguageId = database.LanguageId.ToString()
    };

    public static UpdateDatabaseRequest ToUpdateRpcDto(this Domain.Models.Database database) => new()
    {
        Id = database.Id.ToString(),
        Name = database.Name
    };

    public static Domain.Models.Database? FromRpcDto(this Database? database)
    {
        if (database is null)
            return null;

        var model = new Domain.Models.Database(Guid.Parse(database.Id))
        {
            Name = database.Name,
            LanguageId = Guid.Parse(database.LanguageId)
        };

        var language = database.Language.FromRpcDto();
        if (language is not null)
            model.WithLanguage(language);
        
        if (database.Tables is not null)
            model.WithTables(database.Tables.Map(TableMapper.FromRpcDto));

        return model;
    }
}