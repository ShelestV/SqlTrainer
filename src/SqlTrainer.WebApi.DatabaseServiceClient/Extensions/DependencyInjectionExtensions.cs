using GrpcHelper.DependencyInjection.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.DatabaseServiceClient.Services;

namespace SqlTrainer.WebApi.DatabaseServiceClient.Extensions;

public static class DependencyInjectionExtensions
{
    public static void AddDatabaseServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<ILanguageService, LanguageService>();
        services.AddTransient<IDatabaseService, DatabaseService>();
        services.AddTransient<ITableService, TableService>();
        services.AddTransient<IAttributeService, AttributeService>();
        services.AddTransient<IScriptService, ScriptService>();
        services.AddTransient<ITableDataService, TableDataService>();

        var databaseInfoUri = configuration.GetValue<string>("DATABASE_INFO_SERVICE_URL")!;
        services.AddNotSecureGrpcClient<DatabaseInfoService.LanguageService.LanguageServiceClient>(databaseInfoUri);
        services.AddNotSecureGrpcClient<DatabaseInfoService.DatabaseService.DatabaseServiceClient>(databaseInfoUri);
        services.AddNotSecureGrpcClient<DatabaseInfoService.TableService.TableServiceClient>(databaseInfoUri);
        services.AddNotSecureGrpcClient<DatabaseInfoService.AttributeService.AttributeServiceClient>(databaseInfoUri);
        services.AddNotSecureGrpcClient<DatabaseInfoService.ScriptService.ScriptServiceClient>(databaseInfoUri);
        services.AddNotSecureGrpcClient<DatabaseInfoService.DataService.DataServiceClient>(databaseInfoUri);
    }
}