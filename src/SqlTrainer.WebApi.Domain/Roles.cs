﻿namespace SqlTrainer.WebApi.Domain;

public static class Roles
{
    public const string Admin = "Administrator";
    public const string Teacher = "Teacher";
    public const string Student = "Student";
    public const string SimpleUser = "Simple User";

}