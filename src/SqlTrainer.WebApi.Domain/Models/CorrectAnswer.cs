﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class CorrectAnswer : IdModel, IEquatable<CorrectAnswer>
{
    public required string Body { get; init; }
    public required Guid QuestionId { get; init; }
    public Question? Question { get; private set; }
    
    public CorrectAnswer(Guid id = default) : base(id) {}
    
    public CorrectAnswer WithQuestion(Question question)
    {
        Question = question;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is CorrectAnswer answer && this.Equals(answer);
    public bool Equals(CorrectAnswer? other) => other is not null && other.Id == this.Id;
    public override int GetHashCode() => base.GetHashCode();

    public static bool operator ==(CorrectAnswer? left, CorrectAnswer? right) => left is not null && left.Equals(right);
    public static bool operator !=(CorrectAnswer? left, CorrectAnswer? right) => !(left == right);

    public override string ToString() => $"{Body} ({Question?.Body})";
}
