﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Language : IdModel, IEquatable<Language>
{
    public required string Name { get; init; }
    public required string ShortName { get; init; }
    public required string BaseConnectionString { get; init; }
    
    public Language(Guid id = default) : base(id) { }

    public override bool Equals(object? obj) => obj is Language language && this.Equals(language);
    public bool Equals(Language? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Language? left, Language? right) => left is not null && left.Equals(right);
    public static bool operator !=(Language? left, Language? right) => !(left == right);

    public override string ToString() => Name;
}
