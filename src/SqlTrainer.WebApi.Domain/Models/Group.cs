﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Group : IdModel, IEquatable<Group>
{
    public required string Name { get; init; }
    public IReadOnlyCollection<User>? Users { get; private set; }
    
    public Group(Guid id = default) : base(id)
    {
    }
    
    public Group WithUsers(IReadOnlyCollection<User> users)
    {
        this.Users = users;
        return this;
    }

    public override bool Equals(object? obj) => obj is Group other && this.Equals(other);
    public bool Equals(Group? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Group? left, Group? right) => left is not null && left.Equals(right);
    public static bool operator !=(Group? left, Group? right) => !(left == right);
    
    public override string ToString() => Name;

}