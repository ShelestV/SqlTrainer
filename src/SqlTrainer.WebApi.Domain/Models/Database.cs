using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Database : IdModel, IEquatable<Database>
{
    public required string Name { get; init; }
    public required Guid LanguageId { get; init; }
    public Language? Language { get; private set; }
    public IReadOnlyCollection<Table>? Tables { get; private set; }

    public Database(Guid id = default) : base(id) { }

    public Database WithLanguage(Language language)
    {
        Language = language;
        return this;
    }

    public Database WithTables(IEnumerable<Table> tables)
    {
        Tables = tables.ToHashSet();
        return this;
    }

    public override bool Equals(object? obj) => obj is Database database && this.Equals(database);
    public bool Equals(Database? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name) && other.LanguageId == this.LanguageId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Database? left, Database? right) => left is not null && left.Equals(right);
    public static bool operator !=(Database? left, Database? right) => !(left == right);
    
    public override string ToString() => Name;
}
