﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class TestQuestion : Model, IEquatable<TestQuestion>
{
    public Guid TestId { get; }
    public Guid QuestionId { get; }
    public required double MaxMark { get; init; }
    public Question? Question { get; private set; }
    public Test? Test { get; private set; }
    
    public TestQuestion(Guid testId, Guid questionId)
    {
        TestId = testId;
        QuestionId = questionId;
    }
    
    public TestQuestion WithQuestion(Question question)
    {
        Question = question;
        return this;
    }
    
    public TestQuestion WithTest(Test test)
    {
        Test = test;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is TestQuestion testQuestion && this.Equals(testQuestion);
    public bool Equals(TestQuestion? other) => other is not null && other.TestId == this.TestId && other.QuestionId == this.QuestionId;
    public override int GetHashCode() => HashCode.Combine(QuestionId, TestId);
    
    public static bool operator ==(TestQuestion? left, TestQuestion? right) => left is not null && left.Equals(right);
    public static bool operator !=(TestQuestion? left, TestQuestion? right) => !(left == right);
    
    public override string ToString() => $"{Question?.Body ?? QuestionId.ToString()} ({Test?.Name ?? TestId.ToString()}) - {MaxMark}";
}
