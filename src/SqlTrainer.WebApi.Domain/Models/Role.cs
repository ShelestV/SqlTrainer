﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Role : IdModel, IEquatable<Role>
{
    public required string Name { get; init; }

    public Role(Guid id = default) : base(id)
    {
    }
    
    public override bool Equals(object? obj) => obj is Role other && this.Equals(other);
    public bool Equals(Role? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Role? left, Role? right) => left is not null && left.Equals(right);
    public static bool operator !=(Role? left, Role? right) => !(left == right);
    
    public override string ToString() => Name;

}