﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class User : IdModel, IEquatable<User>
{
    public required string Name { get; init; }
    public required string Email { get; init; }
    public string? HashedPassword { get; init; }
    public required Guid RoleId { get; init; }
    public Role? Role { get; private set; }
    public Guid? GroupId { get; init; }
    public Group? Group { get; private set; }
    public string? FaceImage { get; init; }
    public double Rate { get; init; }
    
    public User(Guid id = default) : base(id)
    {
    }
    
    public User WithRole(Role role)
    {
        this.Role = role;
        return this;
    }
    
    public User WithGroup(Group group)
    {
        this.Group = group;
        return this;
    }

    public override bool Equals(object? obj) => obj is User user && this.Equals(user);
    public bool Equals(User? other) => other is not null && (other.Id == this.Id || other.Email.Equals(this.Email));
    public override int GetHashCode() => base.GetHashCode();

    public static bool operator ==(User? left, User? right) => left is not null && left.Equals(right);
    public static bool operator !=(User? left, User? right) => !(left == right);

    public override string ToString() => $"{Email} ({Name})";

}