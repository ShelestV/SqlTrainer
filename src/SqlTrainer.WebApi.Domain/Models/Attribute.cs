﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Attribute : IdModel, IEquatable<Attribute>
{
    public required string Name { get; init; }
    public required bool IsPrimaryKey { get; init; }
    public required bool IsNotNull { get; init; }
    public required bool IsUnique { get; init; }
    public string? DefaultValue { get; init; }
    public required string Type { get; init; }
    public int? VarcharNumberOfSymbols { get; init; }
    public Guid? ForeignKeyId { get; init; }
    public required Guid TableId { get; init; }
    public required int Order { get; init; }
    public bool IsNew { get; init; } = false;
    public Table? Table { get; private set; }
    public ForeignKey? ForeignKey { get; private set; }
    
    public Attribute(Guid id = default) : base(id) { }
    
    public Attribute WithTable(Table table)
    {
        Table = table;
        return this;
    }
    
    public Attribute WithForeignKey(ForeignKey foreignKey)
    {
        ForeignKey = foreignKey;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is Attribute attribute && this.Equals(attribute);
    public bool Equals(Attribute? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name) && other.TableId == this.TableId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Attribute? left, Attribute? right) => left is not null && left.Equals(right);
    public static bool operator !=(Attribute? left, Attribute? right) => !(left == right);
    
    public override string ToString() => Name;
}
