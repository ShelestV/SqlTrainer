﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class ForeignKey : IdModel, IEquatable<ForeignKey>
{
    public required string Name { get; init; }
    public required Guid TableId { get; init; }
    public required Table Table { get; init; }
    
    public ForeignKey(Guid id = default) : base(id) { }

    public override bool Equals(object? obj) => obj is ForeignKey foreignKey && this.Equals(foreignKey);
    public bool Equals(ForeignKey? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name) && other.TableId == this.TableId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(ForeignKey? left, ForeignKey? right) => left is not null && left.Equals(right);
    public static bool operator !=(ForeignKey? left, ForeignKey? right) => !(left == right);
    
    public override string ToString() => Name;
}
