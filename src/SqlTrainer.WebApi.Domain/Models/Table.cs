﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Table : IdModel, IEquatable<Table>
{
    public required string Name { get; init; }
    public required Guid DatabaseId { get; init; }
    public Database? Database { get; private set; }
    public IReadOnlyCollection<Attribute>? Attributes { get; private set; }
    
    public Table(Guid id = default) : base(id) { }
    
    public Table WithDatabase(Database database)
    {
        Database = database;
        return this;
    }
    
    public Table WithAttributes(IEnumerable<Attribute> attributes)
    {
        Attributes = attributes.ToHashSet();
        return this;
    }
    
    public override bool Equals(object? obj) => obj is Table table && this.Equals(table);
    public bool Equals(Table? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name) && other.DatabaseId == this.DatabaseId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Table? left, Table? right) => left is not null && left.Equals(right);
    public static bool operator !=(Table? left, Table? right) => !(left == right);
    
    public override string ToString() => Name;
}
