﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Test : IdModel, IEquatable<Test>
{
    public required string Name { get; init; }
    public required DateTimeOffset CreatedAt { get; init; }
    public IReadOnlyCollection<TestQuestion>? TestQuestions { get; private set; }

    public Test(Guid id = default) : base(id)
    {
    } 
    
    public Test WithTestQuestions(IReadOnlyCollection<TestQuestion> testQuestions)
    {
        TestQuestions = testQuestions;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is Test test && this.Equals(test);
    public bool Equals(Test? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Test? left, Test? right) => left is not null && left.Equals(right);
    public static bool operator !=(Test? left, Test? right) => !(left == right);
    
    public override string ToString() => Name;
}
