﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class Topic : IdModel, IEquatable<Topic>
{
    public required string Name { get; init; }
    public IReadOnlyCollection<Question>? Questions { get; private set; }
    
    public Topic(Guid id = default) : base(id) {}
    
    public Topic WithQuestions(IReadOnlyCollection<Question> questions)
    {
        Questions = questions;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is Topic topic && this.Equals(topic);
    public bool Equals(Topic? other) => other is not null && (other.Id == this.Id || other.Name.Equals(this.Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Topic? left, Topic? right) => left is not null && left.Equals(right);
    public static bool operator !=(Topic? left, Topic? right) => !(left == right);
    
    public override string ToString() => Name;
}
