﻿using Domain.Models;

namespace SqlTrainer.WebApi.Domain.Models;

public sealed class UserAnswer : Model, IEquatable<UserAnswer>
{
    public Guid ScheduleTestId { get; }
    public Guid QuestionId { get; }
    public Guid UserId { get; }
    public required string Body { get; init; }
    public required double ProgramMark { get; init; }
    public double? TeacherMark { get; init; }
    public double? QuestionMaxMark { get; init; }
    public required DateTimeOffset AnsweredAt { get; init; }
    public ScheduleTest? ScheduleTest { get; private set; }
    public Question? Question { get; private set; }
    public User? User { get; private set; }
    
    public UserAnswer(Guid scheduleTestId, Guid questionId, Guid userId)
    {
        ScheduleTestId = scheduleTestId;
        QuestionId = questionId;
        UserId = userId;
    }
    
    public UserAnswer WithScheduleTest(ScheduleTest scheduleTest)
    {
        ScheduleTest = scheduleTest;
        return this;
    }
    
    public UserAnswer WithQuestion(Question question)
    {
        Question = question;
        return this;
    }
    
    public UserAnswer WithUser(User user)
    {
        User = user;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is UserAnswer userAnswer && this.Equals(userAnswer);
    public bool Equals(UserAnswer? other) => other is not null && other.ScheduleTestId == this.ScheduleTestId && other.QuestionId == this.QuestionId && other.UserId == this.UserId;
    public override int GetHashCode() => HashCode.Combine(ScheduleTestId, QuestionId, UserId);
    
    public static bool operator ==(UserAnswer? left, UserAnswer? right) => left is not null && left.Equals(right);
    public static bool operator !=(UserAnswer? left, UserAnswer? right) => !(left == right);
    
    public override string ToString() => $"User: {UserId}, Question: {Question?.ToString() ?? QuestionId.ToString()}, Schedule Test: {ScheduleTest?.ToString() ?? ScheduleTestId.ToString()} - {ProgramMark}/{TeacherMark}/{QuestionMaxMark}";
}
