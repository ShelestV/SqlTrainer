using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.VerificationServiceClient.Services;

namespace SqlTrainer.WebApi.VerificationServiceClient.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddVerificationService(this IServiceCollection services, IConfiguration configuration)
    {
        var verificationServiceUrl = configuration.GetValue<string>("VERIFICATION_SERVICE_URL")!;
        services.AddHttpClient(Constants.HttpClientNames.VerificationService, c => c.BaseAddress = new Uri(verificationServiceUrl));

        services.AddTransient<IVerificationService, VerificationService>();

        return services;
    }
}