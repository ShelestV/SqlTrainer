using System.Text;

namespace SqlTrainer.WebApi.VerificationServiceClient;

internal static class Constants
{
    public static readonly Encoding Encoding = Encoding.UTF8;
    public const string MediaType = "application/json";
    
    internal static class HttpClientNames
    {
        public const string VerificationService = "VerificationService";
    }
}