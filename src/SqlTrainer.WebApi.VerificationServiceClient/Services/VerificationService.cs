using System.Text.Json;
using System.Text.Json.Serialization;
using Results;
using SqlTrainer.WebApi.Application.Dtos;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.VerificationServiceClient.Services;

public sealed class VerificationService : IVerificationService
{
    private readonly IHttpClientFactory httpClientFactory;
    
    public VerificationService(IHttpClientFactory httpClientFactory)
    {
        this.httpClientFactory = httpClientFactory;
    }
    
    public async Task<Result> SaveAsync(VerificationDto verificationDto, CancellationToken cancellationToken = default) => 
        await MakeRequestAsync(async token => await PostRequestAsync("/extract", verificationDto, token), cancellationToken);

    public async Task<Result> VerifyAsync(VerificationDto verificationDto, CancellationToken cancellationToken = default) => 
        await MakeRequestAsync(async token => await PostRequestAsync("/verify", verificationDto, token), cancellationToken);

    public async Task<Result> DeleteAsync(string email, CancellationToken cancellationToken = default) =>
        await MakeRequestAsync(async token => await DeleteRequestAsync($"/delete/{email}", token), cancellationToken);

    private async Task<Result> MakeRequestAsync(Func<CancellationToken, Task<VerificationServiceResponse?>> makeRequestAsync, CancellationToken cancellationToken = default)
    {
        var result = Result.Create();
        
        var response = await makeRequestAsync(cancellationToken);

        if (response is null)
        {
            result.Fail("Internal exception on verification service. Please, contact the administrator");
            return result;
        }

        if (response.IsSuccess)
        {
            result.Complete();
            return result;
        }
            
        result.Fail(response.Errors);
        return result;
    }

    private async Task<VerificationServiceResponse?> PostRequestAsync(string requestUri, VerificationDto verificationDto, CancellationToken cancellationToken = default)
    {
        var client = httpClientFactory.CreateClient(Constants.HttpClientNames.VerificationService);
        var jsonDto = JsonSerializer.Serialize(verificationDto);
        var jsonContent = new StringContent(jsonDto, Constants.Encoding, Constants.MediaType);

        var response = await client.PostAsync(requestUri, jsonContent, cancellationToken);
        if (500 <= (int)response.StatusCode)
            return null;

        return await ParseResponseAsync(response, cancellationToken);
    }

    private async Task<VerificationServiceResponse?> DeleteRequestAsync(string requestUri, CancellationToken cancellationToken)
    {
        var client = httpClientFactory.CreateClient(Constants.HttpClientNames.VerificationService);
        var response = await client.DeleteAsync(requestUri, cancellationToken);

        if (500 <= (int)response.StatusCode)
            return null;

        return await ParseResponseAsync(response, cancellationToken);
    }

    private static async Task<VerificationServiceResponse> ParseResponseAsync(HttpResponseMessage response, CancellationToken cancellationToken)
    {
        var jsonResponse = await response.Content.ReadAsStringAsync(cancellationToken);

        var apiResponse = JsonSerializer.Deserialize<VerificationServiceResponse>(jsonResponse);
        if (apiResponse is not null)
            return apiResponse;

        return new VerificationServiceResponse
        {
            IsSuccess = false,
            Errors = new[] { "Could not parse response from Verification Service" }
        };
    }
    
    private sealed class VerificationServiceResponse
    {
        [JsonPropertyName("success")]
        public bool IsSuccess { get; init; }
        [JsonPropertyName("errors")]
        public string[] Errors { get; init; } = null!;
    }
}