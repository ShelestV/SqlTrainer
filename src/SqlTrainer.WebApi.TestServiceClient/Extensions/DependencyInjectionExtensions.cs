using GrpcHelper.DependencyInjection.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.WebApi.Application.Services;
using SqlTrainer.WebApi.TestServiceClient.Services;

namespace SqlTrainer.WebApi.TestServiceClient.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddTestServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<ITopicService, TopicService>();
        services.AddTransient<IQuestionService, QuestionService>();
        services.AddTransient<ITestService, TestService>();
        services.AddTransient<IScheduleTestService, ScheduleTestService>();
        services.AddTransient<IUserScheduleTestService, UserScheduleTestService>();
        services.AddTransient<IAnswerService, AnswerService>();

        var testInfoUri = configuration.GetValue<string>("TEST_INFO_SERVICE_URL")!;
        services.AddNotSecureGrpcClient<TestInfoService.TopicService.TopicServiceClient>(testInfoUri);
        services.AddNotSecureGrpcClient<TestInfoService.QuestionService.QuestionServiceClient>(testInfoUri);
        services.AddNotSecureGrpcClient<TestInfoService.TestService.TestServiceClient>(testInfoUri);
        services.AddNotSecureGrpcClient<TestInfoService.ScheduleTestService.ScheduleTestServiceClient>(testInfoUri);
        services.AddNotSecureGrpcClient<TestInfoService.UserScheduleTestService.UserScheduleTestServiceClient>(testInfoUri);
        services.AddNotSecureGrpcClient<TestInfoService.UserAnswerService.UserAnswerServiceClient>(testInfoUri);

        return services;
    }
}