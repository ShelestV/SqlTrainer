using Google.Protobuf.WellKnownTypes;
using GrpcHelper.Extensions;
using Results;
using SqlTrainer.TestInfoService;
using SqlTrainer.WebApi.Application;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.TestServiceClient.Services;

public sealed class ScheduleTestService : IScheduleTestService
{
    private readonly TestInfoService.ScheduleTestService.ScheduleTestServiceClient scheduleTestServiceClient;
    
    public ScheduleTestService(TestInfoService.ScheduleTestService.ScheduleTestServiceClient scheduleTestServiceClient)
    {
        this.scheduleTestServiceClient = scheduleTestServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.ScheduleTest scheduleTest, CancellationToken cancellationToken = default)
    {
        var request = new AddScheduleTestRequest
        {
            TestId = scheduleTest.TestId.ToString(),
            StartAt = scheduleTest.StartAt.ToUniversalTime().ToTimestamp(),
            FinishAt = scheduleTest.FinishAt.ToUniversalTime().ToTimestamp(),
            UserIds = { scheduleTest.UserScheduleTests?.Select(ust => ust.UserId.ToString()) ?? Enumerable.Empty<string>() },
        };
        var response = await scheduleTestServiceClient.AddAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new DeleteScheduleTestRequest { Id = id.ToString() };
        var response = await scheduleTestServiceClient.DeleteAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.ScheduleTest scheduleTest, CancellationToken cancellationToken = default)
    {
        var request = new UpdateScheduleTestRequest
        {
            Id = scheduleTest.Id.ToString(),
            TestId = scheduleTest.TestId.ToString(),
            StartAt = scheduleTest.StartAt.ToUniversalTime().ToTimestamp(),
            FinishAt = scheduleTest.FinishAt.ToUniversalTime().ToTimestamp(),
            UserIds = { scheduleTest.UserScheduleTests?.Select(ust => ust.UserId.ToString()) ?? Enumerable.Empty<string>() },
        };
        var response = await scheduleTestServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<Domain.Models.ScheduleTest>> GetAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new GetByIdScheduleTestRequest { Id = id.ToString() };
        var response = await scheduleTestServiceClient.GetAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.ScheduleTest, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.ScheduleTest>>> GetAllAsync(PaginationFilter pagination, CancellationToken cancellationToken = default)
    {
        var request = new GetAllScheduleTestsRequest
        {
            SearchTerm = pagination.SearchTerm,
            OrderBy = pagination.OrderBy,
            OrderByDirection = pagination.OrderByDirection,
            Page = pagination.Page,
            PageSize = pagination.PageSize,
        };
        var response = await scheduleTestServiceClient.GetAllAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.ScheduleTests, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.ScheduleTest>>> GetByUserIdAsync(Guid userId, PaginationFilter pagination, CancellationToken cancellationToken = default)
    {
        var request = new GetByUserScheduleTestsRequest
        {
            UserId = userId.ToString(),
            SearchTerm = pagination.SearchTerm,
            OrderBy = pagination.OrderBy,
            OrderByDirection = pagination.OrderByDirection,
            Page = pagination.Page,
            PageSize = pagination.PageSize,
        };
        var response = await scheduleTestServiceClient.GetByUserAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.ScheduleTests, Map);
    }

    private static Domain.Models.ScheduleTest? Map(ScheduleTest? scheduleTest)
    {
        if (scheduleTest is null)
            return null;

        var model = new Domain.Models.ScheduleTest(Guid.Parse(scheduleTest.Id))
        {
            TestId = Guid.Parse(scheduleTest.TestId),
            StartAt = scheduleTest.StartAt.ToDateTimeOffset(),
            FinishAt = scheduleTest.FinishAt.ToDateTimeOffset()
        };

        if (scheduleTest.Test is not null)
            model.WithTest(new Domain.Models.Test(model.TestId)
            {
                Name = scheduleTest.Test.Name,
                CreatedAt = DateTimeOffset.UtcNow
            });

        return model;
    }
}