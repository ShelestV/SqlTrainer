using Google.Protobuf.WellKnownTypes;
using GrpcHelper.Extensions;
using Results;
using SqlTrainer.TestInfoService;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.TestServiceClient.Services;

public sealed class AnswerService : IAnswerService
{
    private readonly UserAnswerService.UserAnswerServiceClient userAnswerServiceClient;
    
    public AnswerService(UserAnswerService.UserAnswerServiceClient userAnswerServiceClient)
    {
        this.userAnswerServiceClient = userAnswerServiceClient;
    }
    
    public async Task<Result> AddRangeAsync(Guid scheduleTestId, Guid userId, DateTimeOffset answeredAt, IEnumerable<Domain.Models.UserAnswer> answers, CancellationToken cancellationToken = default)
    {
        var request = new AddUserAnswersRequest
        {
            ScheduleTestId = scheduleTestId.ToString(),
            UserId = userId.ToString(),
            AnsweredAt = answeredAt.ToUniversalTime().ToTimestamp(),
            QuestionBodies =
            {
                answers.Select(a => new QuestionBody
                {
                    QuestionId = a.QuestionId.ToString(),
                    Body = a.Body,
                })
            }
        };
        var response = await userAnswerServiceClient.AddRangeAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> SetTeacherMarkAsync(Guid scheduleTestId, Guid questionId, Guid userId, double teacherMark, CancellationToken cancellationToken = default)
    {
        var request = new UpdateUserAnswerRequest
        {
            ScheduleTestId = scheduleTestId.ToString(),
            QuestionId = questionId.ToString(),
            UserId = userId.ToString(),
            TeacherMark = teacherMark,
        };
        var response = await userAnswerServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.UserAnswer>>> GetByUserAndScheduleTestAsync(Guid userId, Guid scheduleTestId, bool includeCorrectAnswer = false, CancellationToken cancellationToken = default)
    {
        var request = new GetUserAnswersByScheduleTestAndUserRequest
        {
            ScheduleTestId = scheduleTestId.ToString(),
            UserId = userId.ToString(),
            IncludeCorrectAnswers = includeCorrectAnswer,
        };
        var response = await userAnswerServiceClient.GetByScheduleTestAndUserAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.UserAnswers, Map);
    }
    
    private static Domain.Models.UserAnswer? Map(UserAnswer? userAnswer)
    {
        if (userAnswer is null)
            return null;
        
        var model = new Domain.Models.UserAnswer(Guid.Parse(userAnswer.ScheduleTestId), Guid.Parse(userAnswer.QuestionId), Guid.Parse(userAnswer.UserId))
        {
            Body = userAnswer.Body,
            AnsweredAt = userAnswer.AnsweredAt.ToDateTimeOffset(),
            ProgramMark = userAnswer.ProgramMark,
            TeacherMark = userAnswer.TeacherMark,
            QuestionMaxMark = userAnswer.QuestionMaxMark
        };

        var scheduleTest = Map(userAnswer.ScheduleTest);
        if (scheduleTest is not null)
            model.WithScheduleTest(scheduleTest);

        var question = Map(userAnswer.Question);
        if (question is not null)
            model.WithQuestion(question);
        
        return model;
    }

    private static Domain.Models.ScheduleTest? Map(ScheduleTest? scheduleTest)
    {
        if (scheduleTest is null)
            return null;

        var model = new Domain.Models.ScheduleTest(Guid.Parse(scheduleTest.Id))
        {
            TestId = Guid.Parse(scheduleTest.TestId),
            StartAt = scheduleTest.StartAt.ToDateTimeOffset(),
            FinishAt = scheduleTest.FinishAt.ToDateTimeOffset()
        };

        var test = Map(scheduleTest.Test);
        if (test is not null)
            model.WithTest(test);

        return model;
    }

    private static Domain.Models.Test? Map(Test? test)
    {
        if (test is null)
            return null;
        
        var model = new Domain.Models.Test(Guid.Parse(test.Id))
        {
            Name = test.Name,
            CreatedAt = DateTimeOffset.UtcNow
        };
        
        model.WithTestQuestions(test.TestQuestions
            .Select(Map)
            .Where(tq => tq is not null)
            .Select(tq => tq!)
            .ToList());

        return model;
    }

    private static Domain.Models.TestQuestion? Map(TestQuestion? testQuestion)
    {
        if (testQuestion is null)
            return null;

        var model = new Domain.Models.TestQuestion(Guid.Parse(testQuestion.TestId), Guid.Parse(testQuestion.QuestionId))
        {
            MaxMark = testQuestion.MaxMark
        };

        var question = Map(testQuestion.Question);
        if (question is not null)
            model.WithQuestion(question);

        return model;
    }

    private static Domain.Models.Question? Map(Question? question)
    {
        if (question is null)
            return null;
        
        var model = new Domain.Models.Question(Guid.Parse(question.Id))
        {
            Body = question.Body,
            Complexity = question.Complexity,
            TopicId = Guid.Parse(question.TopicId),
            DatabaseId = Guid.Parse(question.DatabaseId)
        };

        var correctAnswer = Map(question.CorrectAnswer);
        if (correctAnswer is not null)
            model.WithCorrectAnswer(correctAnswer);
        
        return model;
    }

    private static Domain.Models.CorrectAnswer? Map(CorrectAnswer? correctAnswer)
    {
        if (correctAnswer is null)
            return null;

        var model = new Domain.Models.CorrectAnswer(Guid.Parse(correctAnswer.Id))
        {
            Body = correctAnswer.Body,
            QuestionId = Guid.Parse(correctAnswer.QuestionId)
        };

        return model;
    }
}