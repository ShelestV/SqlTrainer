﻿using GrpcHelper.Extensions;
using Results;
using SqlTrainer.TestInfoService;
using SqlTrainer.WebApi.Application;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.TestServiceClient.Services;

public sealed class QuestionService : IQuestionService
{
    private readonly TestInfoService.QuestionService.QuestionServiceClient questionServiceClient;
    
    public QuestionService(TestInfoService.QuestionService.QuestionServiceClient questionServiceClient)
    {
        this.questionServiceClient = questionServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.Question question, CancellationToken cancellationToken = default)
    {
        var request = new AddQuestionRequest
        {
            Body = question.Body,
            Complexity = question.Complexity,
            TopicId = question.TopicId.ToString(),
            DatabaseId = question.DatabaseId.ToString(),
            CorrectAnswerBody = question.CorrectAnswer!.Body,
        };
        var response = await questionServiceClient.AddAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new DeleteQuestionRequest { Id = id.ToString() };
        var response = await questionServiceClient.DeleteAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.Question question, CancellationToken cancellationToken = default)
    {
        var request = new UpdateQuestionRequest
        {
            Id = question.Id.ToString(),
            Body = question.Body,
            Complexity = question.Complexity,
            TopicId = question.TopicId.ToString(),
            DatabaseId = question.DatabaseId.ToString(),
            CorrectAnswerId = question.CorrectAnswer!.Id.ToString(),
            CorrectAnswerBody = question.CorrectAnswer!.Body,
        };
        var response = await questionServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Question>>> GetAllAsync(PaginationFilter paginationFilter, bool includeCorrectAnswer = true, CancellationToken cancellationToken = default)
    {
        var request = new GetAllQuestionsRequest
        {
            SearchTerm = paginationFilter.SearchTerm,
            OrderBy = paginationFilter.OrderBy,
            OrderByDirection = paginationFilter.OrderByDirection,
            Page = paginationFilter.Page,
            PageSize = paginationFilter.PageSize,
            IncludeCorrectAnswer = includeCorrectAnswer
        };
        var response = await questionServiceClient.GetAllAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Questions, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Question>>> GetByTestIdAsync(Guid testId, PaginationFilter paginationFilter, bool includeCorrectAnswer = true, CancellationToken cancellationToken = default)
    {
        var request = new GetQuestionsByTestRequest
        {
            TestId = testId.ToString(),
            SearchTerm = paginationFilter.SearchTerm,
            OrderBy = paginationFilter.OrderBy,
            OrderByDirection = paginationFilter.OrderByDirection,
            Page = paginationFilter.Page,
            PageSize = paginationFilter.PageSize,
            IncludeCorrectAnswer = includeCorrectAnswer
        };
        var response = await questionServiceClient.GetByTestAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Questions, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Question>>> GetByScheduleIdAsync(Guid scheduleTestId, bool includeCorrectAnswer = true, CancellationToken cancellationToken = default)
    {
        var request = new GetQuestionsByScheduleTestRequest
        {
            ScheduleTestId = scheduleTestId.ToString(),
            IncludeCorrectAnswer = includeCorrectAnswer
        };
        var response = await questionServiceClient.GetByScheduleTestAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Questions, Map);
    }
    
    private static Domain.Models.Question? Map(Question? question)
    {
        if (question is null)
            return null;
        
        var model = new Domain.Models.Question(Guid.Parse(question.Id))
        {
            Body = question.Body,
            Complexity = question.Complexity,
            TopicId = Guid.Parse(question.TopicId),
            DatabaseId = Guid.Parse(question.DatabaseId)
        };
        
        if (question.CorrectAnswer is not null)
            model.WithCorrectAnswer(new Domain.Models.CorrectAnswer(Guid.Parse(question.CorrectAnswer.Id))
            {
                Body = question.CorrectAnswer.Body,
                QuestionId = model.Id
            });

        return model;
    }
}