using Google.Protobuf.WellKnownTypes;
using GrpcHelper.Extensions;
using Results;
using SqlTrainer.TestInfoService;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.TestServiceClient.Services;

public sealed class UserScheduleTestService : IUserScheduleTestService
{
    private readonly TestInfoService.UserScheduleTestService.UserScheduleTestServiceClient userScheduleTestServiceClient;
    
    public UserScheduleTestService(TestInfoService.UserScheduleTestService.UserScheduleTestServiceClient userScheduleTestServiceClient)
    {
        this.userScheduleTestServiceClient = userScheduleTestServiceClient;
    }
    
    public async Task<Result> UpdateAsync(Guid scheduleTestId, Guid userId, DateTimeOffset? finishedAt, bool? checkedByTeacher, CancellationToken cancellationToken = default)
    {
        var request = new UpdateUserScheduleTestRequest
        {
            ScheduleTestId = scheduleTestId.ToString(),
            UserId = userId.ToString(),
            FinishedAt = finishedAt?.ToUniversalTime().ToTimestamp(),
            CheckedByTeacher = checkedByTeacher,
        };
        var response = await userScheduleTestServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }
}