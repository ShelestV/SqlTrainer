﻿using GrpcHelper.Extensions;
using Results;
using SqlTrainer.TestInfoService;
using SqlTrainer.WebApi.Application;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.TestServiceClient.Services;

public sealed class TopicService : ITopicService
{
    private readonly TestInfoService.TopicService.TopicServiceClient topicServiceClient;
    
    public TopicService(TestInfoService.TopicService.TopicServiceClient topicServiceClient)
    {
        this.topicServiceClient = topicServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.Topic topic, CancellationToken cancellationToken = default)
    {
        var request = new AddTopicRequest { Name = topic.Name };
        var response = await topicServiceClient.AddAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new DeleteTopicRequest { Id = id.ToString() };
        var response = await topicServiceClient.DeleteAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.Topic topic, CancellationToken cancellationToken = default)
    {
        var request = new UpdateTopicRequest { Id = topic.Id.ToString(), Name = topic.Name };
        var response = await topicServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Topic>>> GetAllAsync(PaginationFilter paginationFilter, CancellationToken cancellationToken = default)
    {
        var request = new GetAllTopicsRequest
        {
            SearchTerm = paginationFilter.SearchTerm,
            OrderBy = paginationFilter.OrderBy,
            OrderByDirection = paginationFilter.OrderByDirection,
            Page = paginationFilter.Page,
            PageSize = paginationFilter.PageSize,
        };
        var response = await topicServiceClient.GetAllAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Topics, Map);
    }
    
    private static Domain.Models.Topic? Map(Topic? topic) => topic is null ? null : new Domain.Models.Topic(Guid.Parse(topic.Id))
    {
        Name = topic.Name,
    };
}