﻿using Google.Protobuf.WellKnownTypes;
using GrpcHelper.Extensions;
using Results;
using SqlTrainer.TestInfoService;
using SqlTrainer.WebApi.Application;
using SqlTrainer.WebApi.Application.Services;

namespace SqlTrainer.WebApi.TestServiceClient.Services;

public sealed class TestService : ITestService
{
    private readonly TestInfoService.TestService.TestServiceClient testServiceClient;
    
    public TestService(TestInfoService.TestService.TestServiceClient testServiceClient)
    {
        this.testServiceClient = testServiceClient;
    }
    
    public async Task<Result> AddAsync(Domain.Models.Test test, CancellationToken cancellationToken = default)
    {
        var request = new AddTestRequest
        {
            Name = test.Name,
            CreatedAt = test.CreatedAt.ToUniversalTime().ToTimestamp(),
            Questions = { test.TestQuestions?.Select(tq => new QuestionMaxMark
            {
                QuestionId = tq.QuestionId.ToString(),
                MaxMark = tq.MaxMark
            }) ?? Enumerable.Empty<QuestionMaxMark>() }
        };
        var response = await testServiceClient.AddAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
    {
        var request = new DeleteTestRequest { Id = id.ToString() };
        var response = await testServiceClient.DeleteAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result> UpdateAsync(Domain.Models.Test test, CancellationToken cancellationToken = default)
    {
        var request = new UpdateTestRequest
        {
            Id = test.Id.ToString(),
            Name = test.Name,
            Questions = { test.TestQuestions?.Select(tq => new QuestionMaxMark
            {
                QuestionId = tq.QuestionId.ToString(),
                MaxMark = tq.MaxMark
            }) ?? Enumerable.Empty<QuestionMaxMark>() }
        };
        var response = await testServiceClient.UpdateAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors);
    }

    public async Task<Result<Domain.Models.Test>> GetAsync(Guid id, bool includeQuestions = true, bool includeCorrectAnswers = true, CancellationToken cancellationToken = default)
    {
        var request = new GetByIdTestRequest { Id = id.ToString() };
        var response = await testServiceClient.GetAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Test, Map);
    }

    public async Task<Result<IReadOnlyCollection<Domain.Models.Test>>> GetAllAsync(PaginationFilter paginationFilter, bool includeQuestions = true, bool includeCorrectAnswers = true, CancellationToken cancellationToken = default)
    {
        var request = new GetAllTestsRequest
        {
            SearchTerm = paginationFilter.SearchTerm,
            OrderBy = paginationFilter.OrderBy,
            OrderByDirection = paginationFilter.OrderByDirection,
            Page = paginationFilter.Page,
            PageSize = paginationFilter.PageSize,
            IncludeQuestions = includeQuestions,
            IncludeCorrectAnswers = includeCorrectAnswers,
        };
        
        var response = await testServiceClient.GetAllAsync(request, cancellationToken: cancellationToken);
        return response.ToResult(r => r.IsSuccess, r => r.Errors, r => r.Tests, Map);
    }
    
    private static Domain.Models.Test? Map(Test? test)
    {
        if (test is null)
            return null;
        
        var model = new Domain.Models.Test(Guid.Parse(test.Id))
        {
            Name = test.Name,
            CreatedAt = DateTimeOffset.UtcNow
        };
        
        model.WithTestQuestions(test.TestQuestions
            .Select(Map)
            .Where(tq => tq is not null)
            .Select(tq => tq!)
            .ToList());

        return model;
    }

    private static Domain.Models.TestQuestion? Map(TestQuestion? testQuestion)
    {
        if (testQuestion is null)
            return null;

        var model = new Domain.Models.TestQuestion(Guid.Parse(testQuestion.TestId), Guid.Parse(testQuestion.QuestionId))
        {
            MaxMark = testQuestion.MaxMark
        };

        var question = Map(testQuestion.Question);
        if (question is not null)
            model.WithQuestion(question);

        return model;
    }

    private static Domain.Models.Question? Map(Question? question)
    {
        if (question is null)
            return null;
        
        var model = new Domain.Models.Question(Guid.Parse(question.Id))
        {
            Body = question.Body,
            Complexity = question.Complexity,
            TopicId = Guid.Parse(question.TopicId),
            DatabaseId = Guid.Parse(question.DatabaseId)
        };

        var correctAnswer = Map(question.CorrectAnswer);
        if (correctAnswer is not null)
            model.WithCorrectAnswer(correctAnswer);
        
        return model;
    }

    private static Domain.Models.CorrectAnswer? Map(CorrectAnswer? correctAnswer)
    {
        if (correctAnswer is null)
            return null;

        var model = new Domain.Models.CorrectAnswer(Guid.Parse(correctAnswer.Id))
        {
            Body = correctAnswer.Body,
            QuestionId = Guid.Parse(correctAnswer.QuestionId)
        };

        return model;
    }
}