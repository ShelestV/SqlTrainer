using Results;

namespace SqlTrainer.WebApi.Application.Services;

public interface IScriptService
{
    Task<Result<string>> ExecuteScriptAsync(Guid databaseId, string script, CancellationToken cancellationToken = default);
}