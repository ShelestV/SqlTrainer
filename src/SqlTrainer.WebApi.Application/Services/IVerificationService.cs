﻿using Results;
using SqlTrainer.WebApi.Application.Dtos;

namespace SqlTrainer.WebApi.Application.Services;

public interface IVerificationService
{
    Task<Result> VerifyAsync(VerificationDto verificationDto, CancellationToken cancellationToken = default);
    Task<Result> SaveAsync(VerificationDto verificationDto, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(string email, CancellationToken cancellationToken = default);
}
