using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface ITableService
{
    Task<Result> AddAsync(Table table, CancellationToken cancellationToken);
    Task<Result> DeleteAsync(Guid tableId, CancellationToken cancellationToken);
    Task<Result> UpdateAsync(Table table, CancellationToken cancellationToken);
    Task<Result<Table>> GetAsync(Guid tableId, CancellationToken cancellationToken);
    Task<Result<IReadOnlyCollection<Table>>> GetAllAsync(PaginationFilter pagination, CancellationToken cancellationToken);
    Task<Result<IReadOnlyCollection<Table>>> GetByDatabaseAsync(Guid databaseId, PaginationFilter pagination, CancellationToken cancellationToken);
}