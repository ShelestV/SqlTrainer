﻿using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface IRoleService
{
    Task<Result<Role>> GetByNameAsync(string name, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Role>>> GetAllAsync(CancellationToken cancellationToken = default);
}