﻿using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface IQuestionService
{
    Task<Result> AddAsync(Question question, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result> UpdateAsync(Question question, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Question>>> GetAllAsync(PaginationFilter paginationFilter, bool includeCorrectAnswer = true, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Question>>> GetByTestIdAsync(Guid testId, PaginationFilter paginationFilter, bool includeCorrectAnswer = true, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Question>>> GetByScheduleIdAsync(Guid scheduleTestId, bool includeCorrectAnswer = true, CancellationToken cancellationToken = default);
}