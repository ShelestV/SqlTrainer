﻿using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface IDatabaseService
{
    Task<Result> AddAsync(Database database, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result> UpdateAsync(Database database, CancellationToken cancellationToken = default);
    Task<Result<Database>> GetAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Database>>> GetAllAsync(PaginationFilter pagination, CancellationToken cancellationToken = default);
}