using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface IAnswerService
{
    Task<Result> AddRangeAsync(Guid scheduleTestId, Guid userId, DateTimeOffset answeredAt, IEnumerable<UserAnswer> answers, CancellationToken cancellationToken = default);
    Task<Result> SetTeacherMarkAsync(Guid scheduleTestId, Guid questionId, Guid userId, double teacherMark, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<UserAnswer>>> GetByUserAndScheduleTestAsync(Guid userId, Guid scheduleTestId, bool includeCorrectAnswer = false, CancellationToken cancellationToken = default);
}