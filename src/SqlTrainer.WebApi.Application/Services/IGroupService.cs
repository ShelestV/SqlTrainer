﻿using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface IGroupService
{
    Task<Result> AddAsync(Group group, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result> UpdateAsync(Group group, CancellationToken cancellationToken = default);
    Task<Result<Group>> GetAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Group>>> GetAllAsync(CancellationToken cancellationToken = default);
}