﻿using Results;

namespace SqlTrainer.WebApi.Application.Services;

public interface ITableDataService
{
    public Task<Result> InsertDataAsync(Guid tableId, string jsonData, CancellationToken cancellationToken = default);
    public Task<Result> UpdateDataAsync(Guid tableId, string oldJsonData, string newJsonData, CancellationToken cancellationToken = default);
    public Task<Result> DeleteDataAsync(Guid tableId, string jsonData, CancellationToken cancellationToken = default);
    public Task<Result<string>> SelectDataAsync(Guid tableId, CancellationToken cancellationToken = default);
}