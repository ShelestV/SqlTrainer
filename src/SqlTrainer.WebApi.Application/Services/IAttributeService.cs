using Results;

namespace SqlTrainer.WebApi.Application.Services;

public interface IAttributeService
{
    Task<Result<IReadOnlyCollection<Domain.Models.Attribute>>> GetByTableAsync(Guid tableId, CancellationToken cancellationToken);
    Task<Result<IReadOnlyCollection<Domain.Models.Attribute>>> GetPrimaryKeysByDatabaseAsync(Guid databaseId, CancellationToken cancellationToken);
}