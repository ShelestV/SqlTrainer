using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface IScheduleTestService
{
    Task<Result> AddAsync(ScheduleTest scheduleTest, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result> UpdateAsync(ScheduleTest scheduleTest, CancellationToken cancellationToken = default);
    Task<Result<ScheduleTest>> GetAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<ScheduleTest>>> GetAllAsync(PaginationFilter pagination, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<ScheduleTest>>> GetByUserIdAsync(Guid userId, PaginationFilter pagination, CancellationToken cancellationToken = default);
}