using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface ILanguageService
{
    Task<Result<Language>> GetAsync(Guid languageId, CancellationToken cancellationToken);
    Task<Result<IReadOnlyCollection<Language>>> GetAllAsync(CancellationToken cancellationToken);
}