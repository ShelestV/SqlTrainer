using Results;

namespace SqlTrainer.WebApi.Application.Services;

public interface IUserScheduleTestService
{
    Task<Result> UpdateAsync(Guid scheduleTestId, Guid userId, DateTimeOffset? finishedAt, bool? checkedByTeacher, CancellationToken cancellationToken = default);
}