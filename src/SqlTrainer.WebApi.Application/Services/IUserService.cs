﻿using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface IUserService
{
    Task<Result> AddAsync(User user, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result> UpdateAsync(User user, CancellationToken cancellationToken = default);
    Task<Result<User>> GetAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result<User>> GetByEmailAsync(string email, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<User>>> GetAllStudentsAsync(CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<User>>> GetAllAsync(CancellationToken cancellationToken = default);
}