﻿using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface ITestService
{
    Task<Result> AddAsync(Test test, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result> UpdateAsync(Test test, CancellationToken cancellationToken = default);
    Task<Result<Test>> GetAsync(Guid id, bool includeQuestions = true, bool includeCorrectAnswers = true, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Test>>> GetAllAsync(PaginationFilter paginationFilter, bool includeQuestions = true, bool includeCorrectAnswers = true, CancellationToken cancellationToken = default);
}