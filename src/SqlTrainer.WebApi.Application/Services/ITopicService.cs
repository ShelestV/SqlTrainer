﻿using Results;
using SqlTrainer.WebApi.Domain.Models;

namespace SqlTrainer.WebApi.Application.Services;

public interface ITopicService
{
    Task<Result> AddAsync(Topic topic, CancellationToken cancellationToken = default);
    Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    Task<Result> UpdateAsync(Topic topic, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<Topic>>> GetAllAsync(PaginationFilter paginationFilter, CancellationToken cancellationToken = default);
}