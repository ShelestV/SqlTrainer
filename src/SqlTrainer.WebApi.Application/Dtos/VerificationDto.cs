﻿using System.Text.Json.Serialization;

namespace SqlTrainer.WebApi.Application.Dtos;

public sealed class VerificationDto
{
    [JsonPropertyName("email")]
    public required string Email { get; init; }
    [JsonPropertyName("base64")]
    public required string Base64 { get; init; }
}
