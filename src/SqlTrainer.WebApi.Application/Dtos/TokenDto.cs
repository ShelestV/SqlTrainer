﻿namespace SqlTrainer.WebApi.Application.Dtos;

public sealed class TokenDto
{
    public required string Token { get; init; }
    public required string Role { get; init; }
}