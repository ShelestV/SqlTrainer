﻿namespace SqlTrainer.WebApi.Application;

public sealed class PaginationFilter
{
    private const string Ascending = "asc";
    private const string Descending = "desc";
    private static readonly string[] ValidOrderDirections = { Ascending, Descending };
    
    private const string DefaultOrderBy = "Id";
    private const string DefaultOrderByDirection = Ascending;
    private const int DefaultPage = 1;
    private const int DefaultPageSize = 50;
    
    public string SearchTerm { get; }
    public string OrderBy { get; }
    public string OrderByDirection { get; }
    public int Page { get; }
    public int PageSize { get; }
    
    public PaginationFilter(string? searchTerm, string? orderBy, string? orderByDirection, int? page, int? pageSize)
    {
        SearchTerm = searchTerm ?? string.Empty;
        OrderBy = orderBy?.ToLower() ?? DefaultOrderBy;
        OrderByDirection = orderByDirection is not null && ValidOrderDirections.Contains(orderByDirection, StringComparer.OrdinalIgnoreCase) 
            ? orderByDirection.ToLower() : DefaultOrderByDirection;
        Page = (1 <= page) ? page.Value : DefaultPage;
        PageSize = (1 <= pageSize) ? pageSize.Value : DefaultPageSize;
    }
}