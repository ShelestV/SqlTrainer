FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["src/SqlTrainer.WebApi/*.csproj", "./"]
ARG NUGET_SOURCE
ARG NUGET_USER_NAME
ARG NUGET_USER_PASSWORD
RUN dotnet nuget add source $NUGET_SOURCE --name "gitlab-nuget-source" --username $NUGET_USER_NAME --password $NUGET_USER_PASSWORD --store-password-in-clear-text
RUN dotnet restore "SqlTrainer.WebApi.csproj"
COPY [ "src/", "./" ]
WORKDIR "/src/SqlTrainer.WebApi"
RUN dotnet build "SqlTrainer.WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SqlTrainer.WebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SqlTrainer.WebApi.dll"]